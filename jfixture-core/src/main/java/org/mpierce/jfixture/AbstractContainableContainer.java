/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

/**
 * A containable that is also a container.
 *
 * @param <N> the type that is this object's identifier
 * @param <C> the type of this object's container
 * @param <M> the identifier type that this object's containables use
 * @param <V> the type that this object contains
 */
@NotThreadSafe
abstract class AbstractContainableContainer<N, C, M, V extends Containable<M, ?>> extends AbstractContainer<M, V>
        implements Containable<N, C> {

    /**
     * the identifier of this object in the container
     */
    @NotNull
    private final N identifier;

    /**
     * the container
     */
    @NotNull
    private final C container;

    /**
     * @param container  the container of this object
     * @param identifier the identifier of this object in its container
     */
    AbstractContainableContainer(@NotNull C container, @NotNull N identifier) {
        super();
        this.container = container;

        this.identifier = identifier;
    }

    @Override
    @NotNull
    public final N getIdentifier() {
        return this.identifier;
    }

    @Override
    @NotNull
    public final C getContainer() {
        return this.container;
    }
}
