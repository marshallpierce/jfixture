/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

/**
 * Base class that iterates over every fixture in the fixture group. Relieves subclasses of needing to write the
 * iteration code.
 */
@NotThreadSafe
abstract class FixtureVisitor extends ContextAware {

    /**
     * @param fixtureGroupRepos the repos to traverse
     *
     * @throws FixtureException
     */
    @SuppressWarnings({"ConstantConditions"})
    final void traverse(@NotNull FixtureGroupRepository fixtureGroupRepos) {
        for (DbIdentifier fixtureGroupName : fixtureGroupRepos.keySet()) {
            this.pushContext("fixtureGroupName=" + fixtureGroupName);

            try {
                FixtureGroup fixtureGroup = fixtureGroupRepos.get(fixtureGroupName);
                for (FixtureIdentifier fixtureName : fixtureGroup.keySet()) {
                    this.pushContext("fixtureName=" + fixtureName);

                    try {
                        Fixture fixture = fixtureGroup.get(fixtureName);
                        for (TableIdentifier tableIdentifier : fixture.keySet()) {
                            this.pushContext("tableName=" + tableIdentifier);

                            try {
                                RowBundle<?> rowBundle = fixture.get(tableIdentifier);
                                for (Row row : rowBundle.getRows()) {
                                    this.pushContext("row=" + row.getRowIdentifier().toString());
                                    this.onRow(row);
                                    try {
                                        for (ColumnIdentifier colName : row.keySet()) {
                                            this.pushContext("column=" + colName);

                                            try {
                                                Value value = row.get(colName);
                                                this.pushContext("value=" + value.asString());

                                                try {
                                                    onValue(fixtureGroupName, row, value);
                                                } finally {
                                                    this.popContext();
                                                }
                                            } finally {
                                                this.popContext();
                                            }
                                        }
                                    } finally {
                                        this.popContext();
                                    }
                                }
                            } finally {
                                this.popContext();
                            }
                        }
                    } finally {
                        this.popContext();
                    }
                }
            } finally {
                this.popContext();
            }
        }
    }

    abstract void onRow(@NotNull Row row);

    /**
     * @return the results, if any, of visiting every value
     */

    /**
     * Perform implementation specific actions on the value. This will be called for every value in the FixtureGroup.
     *
     * @param dbName the db that this row is in
     * @param row    the row containing the value being visited
     * @param value  the value being visited
     */
    abstract void onValue(@NotNull DbIdentifier dbName, @NotNull Row row, @NotNull Value value);
}
