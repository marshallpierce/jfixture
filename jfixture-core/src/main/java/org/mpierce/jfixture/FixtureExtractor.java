/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Generates a FixtureGroup containing unresolved fixture data. That is, if a column had a value of
 * "${someFixture.tbl.col}", that string is exactly what would be returned in the corresponding Value object.
 */
@NotThreadSafe
final class FixtureExtractor extends ContextAware {

    /**
     * Construct a FixtureGroup without doing any resolution of special values.
     *
     * @param fixtureGroupRepository the repository that the resulting fixture group should be in
     * @param dbName                 the database name that this fixture group should be inserted into
     * @param parsedYaml             the yaml structure from the fixture file
     *
     * @return a FixtureGroup without resolved references
     */
    @SuppressWarnings("unchecked")
    FixtureGroup extractUnresolvedFixtures(FixtureGroupRepository fixtureGroupRepository, DbIdentifier dbName,
            Object parsedYaml) {
        if (!(parsedYaml instanceof Map)) {
            throw this.fail("Top-level YAML structure is not a Map");
        }

        Map<String, Object> rawFixtures = (Map) parsedYaml;

        // yaml map keys are always strings
        Set<String> fixtureNames = rawFixtures.keySet();

        FixtureGroup fixtureGroup = new FixtureGroup(fixtureGroupRepository, dbName);

        for (String fixtureName : fixtureNames) {
            this.pushContext("fixtureName=" + fixtureName);

            Fixture fixture = new Fixture(fixtureGroup, new FixtureIdentifier(fixtureName));
            fixtureGroup.add(fixture);
            try {
                Object fixtureObj = rawFixtures.get(fixtureName);
                if (!(fixtureObj instanceof Map)) {
                    throw this.fail("Fixture is not a Map");
                }

                Map<String, Object> rawFixture = (Map) fixtureObj;

                // for each table in the fixture
                for (Map.Entry<String, Object> stringObjectEntry : rawFixture.entrySet()) {
                    String tableName = stringObjectEntry.getKey();
                    Object tableObj = stringObjectEntry.getValue();
                    this.pushContext("tableName=" + tableName);
                    try {
                        fixture.add(this.getTable(fixture, new TableIdentifier(tableName), tableObj));
                    } finally {
                        this.popContext();
                    }
                }
            } finally {
                this.popContext();
            }
        }

        return fixtureGroup;
    }

    /**
     * @param fixture         the fixture this table will be a part of
     * @param tableIdentifier the name of the table
     * @param tableObj        the yaml object representing a table
     *
     * @return a RowBundle object
     */
    @SuppressWarnings("unchecked")
    private RowBundle<?> getTable(Fixture fixture, TableIdentifier tableIdentifier, Object tableObj) {

        // at this point, there are three options. Either the table has one row,
        // or a list of rows, or a map of named rows.
        if (tableObj instanceof List) {
            // it's a list of rows
            this.pushContext("tableType=rowList");
            try {
                List tableObjList = (List) tableObj;
                List<Map<String, String>> stringMapList = new ArrayList<Map<String, String>>();

                for (int i = 0; i < tableObjList.size(); i++) {
                    this.pushContext("rowIndex=" + i);
                    try {
                        Object rowObj = tableObjList.get(i);

                        stringMapList.add(this.getStringRowStructure(rowObj));
                    } finally {
                        this.popContext();
                    }
                }

                RowListBundle bundle = new RowListBundle(fixture, tableIdentifier);
                List<Row> rowList = this.getRowList(bundle, stringMapList);
                bundle.setContents(rowList);
                return bundle;
            } finally {
                this.popContext();
            }
        } else if (tableObj instanceof Map) {
            // it's either a map of named rows or a single row

            Map<String, Object> tableMap = (Map) tableObj;

            if (tableMap.isEmpty()) {
                throw this.fail("The table did not contain anything");
            }

            Object mapEntry = tableMap.values().iterator().next();

            if (mapEntry instanceof Map) {
                this.pushContext("tableType=namedRows");
                try {
                    // named rows

                    Map<String, Map<String, String>> stringRowMap = new HashMap<String, Map<String, String>>();
                    // for each row key, get the row map object
                    for (Map.Entry<String, Object> stringObjectEntry : tableMap.entrySet()) {
                        String tableKey = stringObjectEntry.getKey();
                        Object tableRowMapEntryObj = stringObjectEntry.getValue();
                        this.pushContext("rowKey=" + tableKey);
                        try {
                            Map<String, String> rowMapEntry = this.getStringRowStructure(tableRowMapEntryObj);

                            stringRowMap.put(tableKey, rowMapEntry);
                        } finally {
                            this.popContext();
                        }
                    }

                    RowMapBundle bundle = new RowMapBundle(fixture, tableIdentifier);
                    bundle.setContents(this.getRowMap(bundle, stringRowMap));
                    return bundle;
                } finally {
                    this.popContext();
                }
            } else if (mapEntry instanceof String) {
                // one implicit row
                this.pushContext("tableType=oneRow");
                try {
                    AnonymousRowBundle bundle = new AnonymousRowBundle(fixture, tableIdentifier);
                    Row row = getRow(bundle, new AnonymousRowIdentifier(), this.getStringRowStructure(tableMap));
                    bundle.setContents(row);

                    return bundle;
                } finally {
                    this.popContext();
                }
            } else {
                throw this.fail("The table did not contain Map or String values: " + String.valueOf(mapEntry));
            }
        } else {
            throw this.fail("The table is neither a List nor a Map:" + String.valueOf(tableObj));
        }
    }

    /**
     * Get a Row version of a Map without resolving any <<>> references
     *
     * @param rowBundle     the rowBundle this row is in
     * @param identifier    the row identifier that the row should use
     * @param stringRowData a map of columns to
     *
     * @return a row containing exactly the values passed in without any manipulation
     */
    private static Row getRow(@NotNull RowBundle<?> rowBundle, @NotNull RowIdentifier identifier,
            @NotNull Map<String, String> stringRowData) {
        Row row = new Row(rowBundle, identifier);

        for (Map.Entry<String, String> colValuePair : stringRowData.entrySet()) {
            Value v = new Value(row, new ColumnIdentifier(colValuePair.getKey()), colValuePair.getValue());
            row.add(v);
        }

        return row;
    }

    /**
     * Convert a list of string-string maps into a list of rows
     *
     * @param rowBundle     the rowBundle that contains these rows
     * @param stringRowList a list of string row representations
     *
     * @return a list of Row objects
     */
    private List<Row> getRowList(RowBundle<?> rowBundle, List<Map<String, String>> stringRowList) {
        List<Row> rowList = new ArrayList<Row>();

        int i = 0;
        for (Map<String, String> rowStringData : stringRowList) {
            this.pushContext("rowIndex=" + i);
            try {
                rowList.add(getRow(rowBundle, new IndexedRowIdentifier(i), rowStringData));
                i++;
            } finally {
                this.popContext();
            }
        }

        return rowList;
    }

    /**
     * Convert the string map representation of a group of named rows into a map of row names to row objects.
     *
     * @param rowBundle        the rowBundle that contains the rows
     * @param stringRowDataMap map of row names to row data
     *
     * @return a map of row names to row objects
     */
    private Map<String, Row> getRowMap(RowBundle<?> rowBundle, Map<String, Map<String, String>> stringRowDataMap) {
        Map<String, Row> rowMap = new HashMap<String, Row>();

        for (Map.Entry<String, Map<String, String>> stringMapEntry : stringRowDataMap.entrySet()) {
            String rowName = stringMapEntry.getKey();
            this.pushContext("rowName=" + rowName);
            try {
                rowMap.put(rowName, getRow(rowBundle, new NamedRowIdentifier(rowName), stringMapEntry.getValue()));
            } finally {
                this.popContext();
            }
        }

        return rowMap;
    }

    /**
     * Type-check and extract String keys and values from what we expect to be a row structure.
     *
     * @param rowObj an Object extracted from yaml
     *
     * @return the input map, type-checked as String to String.
     *
     * @throws FixtureException
     * @throws IllegalArgumentException
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> getStringRowStructure(Object rowObj) {
        if (!(rowObj instanceof Map)) {
            throw this.fail("The row object was not a map");
        }

        Map rawRowMap = (Map) rowObj;

        Map<String, String> typedMap = new HashMap<String, String>();

        for (Object keyObj : rawRowMap.keySet()) {
            if (!(keyObj instanceof String)) {
                throw this.fail("Encountered a key that wasn't a string");
            }

            String keyString = (String) keyObj;
            this.pushContext("column=" + keyString);
            try {
                Object valObj = rawRowMap.get(keyString);
                if (!(valObj instanceof String)) {
                    throw this.fail("Encountered a value that wasn't a string: " + valObj);
                }

                String valString = (String) valObj;

                typedMap.put(keyString, valString);
            } finally {
                this.popContext();
            }
        }

        return typedMap;
    }
}
