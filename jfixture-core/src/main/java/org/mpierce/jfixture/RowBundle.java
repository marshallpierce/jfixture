/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Set;

/**
 * A group of one or more rows. There are three kinds of row bundles: ones with a single row, ones with a list of rows
 * and ones with named rows. This defines the common interface for all three.
 *
 * Bundles are guaranteed to not contain duplicate rows.
 *
 * A RowBundle represents a collection of rows in a particular fixture that are all in the same DB table. There can be
 * multiple RowBundle instances that contain rows that all end up in the same DB table.
 *
 * @param <T> the type of thing this table contains (row map, row list, etc)
 */
@NotThreadSafe
abstract class RowBundle<T> extends AbstractContainable<TableIdentifier, Fixture> {

    @NotNull
    private final RowIdentifierType type;

    /**
     * the contents of this table (row list, row map, etc)
     */
    T contents = null;

    /**
     * @param fixture    the fixture that this table is in
     * @param identifier the name of the table in its containing fixture
     * @param type       the type of row identifier that should be used to access the contents of this table
     */
    RowBundle(@NotNull Fixture fixture, @NotNull TableIdentifier identifier, @NotNull RowIdentifierType type) {
        super(identifier, fixture);

        this.type = type;
    }

    /**
     * @return the rows in the table, regardless of table type
     */
    abstract Set<Row> getRows();

    /**
     * @return the number of rows in the table
     */
    abstract int size();

    /**
     * @return the type of table
     */
    @NotNull
    final RowIdentifierType getType() {
        return this.type;
    }

    /**
     * @param name the identifier to look up
     *
     * @return the row
     *
     * @throws FixtureException if the row can't be found
     */
    @NotNull
    public final Row get(@NotNull RowIdentifier name) {
        Row row = this.getOrNull(name);

        if (row == null) {
            throw new FixtureException("Could not find row for identifier " + name);
        }

        return row;
    }

    @Nullable
    public final Row getOrNull(@NotNull RowIdentifier name) {

        this.checkContentsSet();
        if (name.getType() != this.getType()) {
            throw new FixtureException(
                    "Tried to access a " + this.type + " row bundle with a " + name.getType() + " identifier");
        }

        switch (name.getType()) {
            case ANONYMOUS_ROW:
                return this.getAnonymousRow();
            case INDEXED_ROW:
                return this.getRowAtIndex(name.getRowPosition());
            case NAMED_ROW:
                return this.getRowWithName(name.getRowName());
        }

        throw new IllegalStateException("Impossible");
    }

    /**
     * Do not use this directly.
     *
     * Only valid for single-row tables.
     *
     * @return the row for the table
     */
    @NotNull
    Row getAnonymousRow() {
        throw new UnsupportedOperationException();
    }

    /**
     * Do not use this directly.
     *
     * Only valid for row-list tables.
     *
     * @param index the row index
     *
     * @return the row at the specified index, or null if the index does not exist
     */
    @Nullable
    Row getRowAtIndex(int index) {
        throw new UnsupportedOperationException();
    }

    /**
     * Do not use this directly.
     *
     * Only valid for named-row tables.
     *
     * @param rowName the row name
     *
     * @return the row with that name, or null if the name does not existf
     */
    @Nullable
    Row getRowWithName(String rowName) {
        throw new UnsupportedOperationException();
    }

    /**
     * A defensive copy is NOT made. This must be an ownership transfer of the contents.
     *
     * @param contents the rows that this table contains
     */
    void setContents(T contents) {
        if (this.contents != null) {
            throw new IllegalStateException();
        }

        this.contents = contents;
    }

    /**
     * If this method returns normally, contents have been set
     */
    @SuppressWarnings({"NonBooleanMethodNameMayNotStartWithQuestion"})
    void checkContentsSet() {
        if (this.contents == null) {
            throw new IllegalStateException("Contents not set");
        }
    }
}
