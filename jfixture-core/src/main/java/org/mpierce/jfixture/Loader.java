/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Loads a parsed yaml structure into the database and constructs the new fixture structure with the data that was
 * actually inserted.
 */
@NotThreadSafe
final class Loader extends ContextAware {

    private final List<Row> rows;
    private final Map<Row, Value> autoIdValues;
    private final Map<Value, ColumnReference> valuesWithRefs;
    private final ConnectionProviderRepository connectionProviderRepos;
    private final FixtureGroupRepository insertedRowsRepos;
    private final ColumnReferenceResolver insertedRowsResolver;

    private final Map<ConnectionProviderWrapper, Set<TableIdentifier>> populatedTables =
            new HashMap<ConnectionProviderWrapper, Set<TableIdentifier>>();

    Loader(List<Row> rows, Map<Row, Value> autoIdValues, Map<Value, ColumnReference> valuesWithRefs,
            ConnectionProviderRepository connectionProviderRepos, FixtureGroupRepository originalRepos) {
        this.connectionProviderRepos = connectionProviderRepos;
        this.rows = rows;
        this.autoIdValues = autoIdValues;
        this.valuesWithRefs = valuesWithRefs;
        this.insertedRowsRepos = createEmptyFixtureStructure(originalRepos);
        this.insertedRowsResolver = new ColumnReferenceResolver(this.insertedRowsRepos, this.context);
    }

    void insertRows() {
        for (Row row : this.rows) {
            this.pushContext("inserting row=" + row);
            try {
                this.handleRow(row);
            } finally {
                this.popContext();
            }
        }
    }

    void clearTables() {
        for (Map.Entry<ConnectionProviderWrapper, Set<TableIdentifier>> entry : this.populatedTables.entrySet()) {
            ConnectionProviderWrapper connectionProvider = entry.getKey();
            Set<TableIdentifier> tables = entry.getValue();
            this.pushContext("db " + connectionProvider.getIdentifier());
            try {
                for (TableIdentifier table : tables) {
                    this.pushContext("table " + table);
                    try {
                        ClearTableSqlAction action = new ClearTableSqlAction(this.context, table);
                        connectionProvider.getConnectionProvider().performAction(action);
                    } finally {
                        this.popContext();
                    }
                }
            } finally {
                this.popContext();
            }
        }
    }

    /**
     * @return the ref resolver for the fixture group repository containing the inserted rows
     */
    ColumnReferenceResolver getResolver() {
        return this.insertedRowsResolver;
    }

    /**
     * @param row the row from the original structure
     */
    private void handleRow(Row row) {
        Value autoIdValue = this.autoIdValues.get(row);
        if (autoIdValue != null) {
            logger.trace("Detected auto id value for row " + row + " in column " + autoIdValue.getIdentifier());
        }

        Map<ColumnIdentifier, String> rowData = new HashMap<ColumnIdentifier, String>();

        for (Map.Entry<ColumnIdentifier, Value> stringValueEntry : row.entrySet()) {
            ColumnIdentifier column = stringValueEntry.getKey();
            Value value = stringValueEntry.getValue();

            if (value == autoIdValue) {
                // this row doesn't actually get inserted; we pull its value back from jdbc post insert
                continue;
            }

            ColumnReference refForValue = this.valuesWithRefs.get(value);
            String effectiveValue;
            if (refForValue != null) {
                // resolve the ref in the inserted rows to see what should be inserted here
                Value resolvedValue = this.insertedRowsResolver.resolve(refForValue);
                effectiveValue = resolvedValue.asString();
            } else {
                // just use what came from the yaml
                effectiveValue = value.asString();
            }

            rowData.put(column, effectiveValue);
        }

        ConnectionProviderWrapper connectionProviderWrapper =
                this.connectionProviderRepos.get(row.getContainer().getContainer().getContainer().getIdentifier());
        Map<ColumnIdentifier, String> inserted =
                this.writeData(connectionProviderWrapper, row.getContainer().getIdentifier(), rowData, autoIdValue);


        Row newRow = getNewRowForRowToInsert(row);

        for (Map.Entry<ColumnIdentifier, String> insertedEntry : inserted.entrySet()) {
            ColumnIdentifier colName = insertedEntry.getKey();
            String value = insertedEntry.getValue();

            newRow.add(new Value(newRow, colName, value));
        }
    }

    /**
     * @param origRow a row in the original structure
     *
     * @return the table that should hold that row's new equivalent
     */
    @NotNull
    private RowBundle<?> getTableToPutNewRowInto(Row origRow) {
        FixtureGroup group =
                this.insertedRowsRepos.get(origRow.getContainer().getContainer().getContainer().getIdentifier());
        Fixture fixture = group.get(origRow.getContainer().getContainer().getIdentifier());
        return fixture.get(origRow.getContainer().getIdentifier());
    }

    /**
     * @param origRow the row in the original structure
     *
     * @return a row in the new structure
     */
    @NotNull
    private Row getNewRowForRowToInsert(Row origRow) {
        RowBundle<?> newRowBundle = this.getTableToPutNewRowInto(origRow);

        Row newRow = new Row(newRowBundle, origRow.getRowIdentifier());

        switch (newRow.getRowIdentifier().getType()) {

            case ANONYMOUS_ROW:
                ((AnonymousRowBundle) newRowBundle).setContents(newRow);
                break;
            case INDEXED_ROW:
                ((RowArrayBundle) newRowBundle).set(newRow);
                break;
            case NAMED_ROW:
                ((RowMapBundle) newRowBundle).set(newRow);
                break;
        }

        return newRow;
    }

    /**
     * @param connectionProviderWrapper the connection provider to use
     * @param tableIdentifier           the table to insert into
     * @param stringRowData             the columns and values to insert, not including the auto id column if any
     * @param autoIdValue               the value that should be replaced with the auto id, or null if none in this row
     *
     * @return the map of data that was inserted for the row, including auto id if any
     */
    private Map<ColumnIdentifier, String> writeData(@NotNull ConnectionProviderWrapper connectionProviderWrapper,
            @NotNull TableIdentifier tableIdentifier, @NotNull Map<ColumnIdentifier, String> stringRowData,
            @Nullable Value autoIdValue) {

        if (stringRowData.size() == 0 && autoIdValue == null) {
            throw this.fail("Empty row");
        }

        StringBuilder sqlBuf = new StringBuilder();

        sqlBuf.append("INSERT INTO ");
        sqlBuf.append(tableIdentifier);
        sqlBuf.append("(");

        // need a consistent order since it's convenient to be able to iterate multiple times
        LinkedHashSet<Map.Entry<ColumnIdentifier, String>> orderedRowData =
                new LinkedHashSet<Map.Entry<ColumnIdentifier, String>>(stringRowData.entrySet());

        Iterator<Map.Entry<ColumnIdentifier, String>> columnIter = orderedRowData.iterator();

        StringBuilder valueTemplateBuf = new StringBuilder();

        while (columnIter.hasNext()) {
            Map.Entry<ColumnIdentifier, String> col = columnIter.next();

            ColumnIdentifier colName = col.getKey();

            sqlBuf.append(colName.toString());
            valueTemplateBuf.append("?");

            if (columnIter.hasNext()) {
                sqlBuf.append(", ");
                valueTemplateBuf.append(", ");
            }
        }

        sqlBuf.append(") VALUES (");
        sqlBuf.append(valueTemplateBuf);
        sqlBuf.append(")");

        // will be set if an auto increment column was in the fixture

        // see http://dmlloyd.blogspot.com/2008/07/proper-resource-management.html for details on
        // this try/catch/finally structure

        SqlAction<Integer> insertAction;
        if (autoIdValue == null) {
            insertAction = new InsertSqlAction(this.context, sqlBuf.toString(), orderedRowData);
        } else {
            insertAction = new InsertSqlAction(this.context, sqlBuf.toString(), orderedRowData, autoIdValue);
        }

        Integer lastInsertedId = connectionProviderWrapper.getConnectionProvider().performAction(insertAction);

        // now we have the auto increment value, if there was one

        Map<ColumnIdentifier, String> insertedData = new HashMap<ColumnIdentifier, String>(stringRowData);


        if (autoIdValue != null) {
            insertedData.put(autoIdValue.getIdentifier(), "" + lastInsertedId);
        }

        this.recordTableUsage(connectionProviderWrapper, tableIdentifier);

        return insertedData;
    }

    /**
     * @param originalRepos the repos to copy the structure of
     *
     * @return a fixture group repository populated down to the level of tables (tables exist, but no rows in the
     *         tables)
     */
    @NotNull
    private static FixtureGroupRepository createEmptyFixtureStructure(FixtureGroupRepository originalRepos) {
        FixtureGroupRepository newRepos = new FixtureGroupRepository();

        for (FixtureGroup fixtureGroup : originalRepos.values()) {
            FixtureGroup newFixtureGroup = new FixtureGroup(newRepos, fixtureGroup.getIdentifier());
            newRepos.add(newFixtureGroup);

            for (Fixture fixture : fixtureGroup.values()) {
                Fixture newFixture = new Fixture(newFixtureGroup, fixture.getIdentifier());
                newFixtureGroup.add(newFixture);

                for (RowBundle<?> rowBundle : fixture.values()) {
                    switch (rowBundle.getType()) {
                        case ANONYMOUS_ROW:
                            newFixture.add(new AnonymousRowBundle(newFixture, rowBundle.getIdentifier()));
                            break;
                        case INDEXED_ROW:
                            RowArrayBundle arrayBundle = new RowArrayBundle(newFixture, rowBundle.getIdentifier());
                            newFixture.add(arrayBundle);
                            arrayBundle.setContents(new Row[rowBundle.size()]);
                            break;
                        case NAMED_ROW:
                            RowMapBundle mapBundle = new RowMapBundle(newFixture, rowBundle.getIdentifier());
                            newFixture.add(mapBundle);
                            mapBundle.setContents(new HashMap<String, Row>());
                            break;
                    }
                }
            }
        }

        return newRepos;
    }

    /**
     * Record a table as being inserted into for later clearing
     *
     * @param connectionProviderWrapper the provider containing the table
     * @param tableIdentifier           the name of the table
     */
    private void recordTableUsage(ConnectionProviderWrapper connectionProviderWrapper,
            TableIdentifier tableIdentifier) {
        Set<TableIdentifier> tableIdentifiers = this.populatedTables.get(connectionProviderWrapper);
        if (tableIdentifiers == null) {
            tableIdentifiers = new HashSet<TableIdentifier>();
            this.populatedTables.put(connectionProviderWrapper, tableIdentifiers);
        }
        tableIdentifiers.add(tableIdentifier);
    }
}
