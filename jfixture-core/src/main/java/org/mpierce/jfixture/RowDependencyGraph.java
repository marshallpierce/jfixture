/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A mutable graph where row dependencies are assembled.
 */
@NotThreadSafe
final class RowDependencyGraph {

    /**
     * map of rows to their dependency nodes
     */
    private final Map<Row, RowDependencyNode> rowNodeMap = new HashMap<Row, RowDependencyNode>();

    /**
     * set of root nodes. All nodes that are created are considered root nodes until an edge is added that ends with
     * them. Thus, this contains nodes that are not depended on.
     */
    private final Set<RowDependencyNode> rootNodes = new HashSet<RowDependencyNode>();

    /**
     * Add a dependency from the start row on the end row.
     *
     * @param start the row that depends on end.
     * @param end   the row that is depended on by start.
     */
    void addDependency(Row start, Row end) {
        RowDependencyNode startNode = this.getNodeForRow(start);
        RowDependencyNode endNode = this.getNodeForRow(end);

        if (startNode.getDependencies().contains(endNode)) {
            /*
             * we already know about this dependency, so ignore. Dupe dependencies can happen if there are 2 refs to
             * the same row.
             */

            return;
        }

        RowDependencyEdge edge = new RowDependencyEdge(startNode, endNode);
        startNode.addOutgoing(edge);
        endNode.addIncoming(edge);

        this.checkForCycles();

        this.rootNodes.remove(endNode);
    }

    /**
     * If this method returns, there are no cycles in the graph.
     */
    @SuppressWarnings({"NonBooleanMethodNameMayNotStartWithQuestion"})
    private void checkForCycles() {
        LinkedHashSet<RowDependencyNode> path = new LinkedHashSet<RowDependencyNode>();

        for (RowDependencyNode rootNode : this.rootNodes) {
            path.clear();

            this.checkForCyclesRecurse(path, rootNode);
        }
    }

    /**
     * @param path        the current path from a root node
     * @param currentNode the node to examine (not currently in path -- a dependency of the last thing in the path)
     */
    @SuppressWarnings({"NonBooleanMethodNameMayNotStartWithQuestion"})
    private void checkForCyclesRecurse(LinkedHashSet<RowDependencyNode> path, RowDependencyNode currentNode) {
        path.add(currentNode);
        try {
            for (RowDependencyNode dep : currentNode.getDependencies()) {
                if (path.contains(dep)) {
                    // found a cycle
                    StringBuilder buf = new StringBuilder();
                    for (RowDependencyNode cycleNode : path) {
                        if (buf.length() > 0) {
                            buf.append(" -> ");
                        }
                        buf.append(cycleNode.getRow().toString());
                    }

                    throw new FixtureException(
                            "Cyclic dependency detected: " + buf + " depends on " + dep.getRow().toString() +
                                    " which already is in the path");
                }

                // this dep node did not cause a cycle, so look at its dependencies
                this.checkForCyclesRecurse(path, dep);
            }
        } finally {
            path.remove(currentNode);
        }
    }

    /**
     * This should only be called once all dependencies have been added.
     *
     * @return a list of rows in the order in which they are to be inserted
     */
    List<Row> getRowsInInsertionOrder() {

        LinkedHashSet<RowDependencyNode> inverseOrderNodes = new LinkedHashSet<RowDependencyNode>();

        // the current 'depth' of nodes being examined
        Set<RowDependencyNode> currentSet = new HashSet<RowDependencyNode>();

        // the dependencies of the current set
        Set<RowDependencyNode> nextSet = new HashSet<RowDependencyNode>();

        currentSet.addAll(this.rootNodes);

        // an individual node may be encountered more than once. We need to honor the LAST time it is encountered.

        while (currentSet.size() > 0) {
            /* if we've seen any of the current set before, we're now finding them deeper down the subtree,
             * so remove them and add them at the end
             */
            inverseOrderNodes.removeAll(currentSet);
            inverseOrderNodes.addAll(currentSet);

            for (RowDependencyNode rowDependencyNode : currentSet) {
                nextSet.addAll(rowDependencyNode.getDependencies());
            }

            // prepare for the next iteration
            currentSet.clear();
            currentSet.addAll(nextSet);
            nextSet.clear();
        }

        List<Row> toInsert = new ArrayList<Row>();

        for (RowDependencyNode inverseOrderNode : inverseOrderNodes) {
            toInsert.add(inverseOrderNode.getRow());
        }

        Collections.reverse(toInsert);

        return toInsert;
    }

    /**
     * Get the dependency node for the row. If the node doesn't exist yet, it will be created.
     *
     * @param row the row to get the node for
     *
     * @return the row dependency node for that row
     */
    @NotNull
    private RowDependencyNode getNodeForRow(@NotNull Row row) {
        //noinspection ConstantConditions
        if (row == null) {
            throw new NullPointerException();
        }

        if (!this.rowNodeMap.containsKey(row)) {
            // first time this row has been seen in the graph
            RowDependencyNode node = new RowDependencyNode(row);

            this.rootNodes.add(node);
            this.rowNodeMap.put(row, node);
        }

        return this.rowNodeMap.get(row);
    }
}
