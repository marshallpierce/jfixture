/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

/**
 * Used when collecting rows that are being inserted into the new fixture structure. We need an array instead of a list
 * because we expect a fixed size and we don't want subsequent insertions to adjust position of any other elements.
 */
@NotThreadSafe
final class RowArrayBundle extends RowBundle<Row[]> {

    /**
     * @param fixture    the fixture that contains this object
     * @param identifier the table name
     */
    RowArrayBundle(Fixture fixture, TableIdentifier identifier) {
        super(fixture, identifier, RowIdentifierType.INDEXED_ROW);
    }

    @SuppressWarnings({"RefusedBequest"})
    @Override
    Row getRowAtIndex(int index) {
        if (this.contents.length <= index) {
            return null;
        }

        return this.contents[index];
    }

    @Override
    Set<Row> getRows() {
        throw new UnsupportedOperationException();
    }

    @Override
    int size() {
        throw new UnsupportedOperationException();
    }

    /**
     * This is only to be used when the table must be added to incrementally (during construction of the resolved
     * fixture structure)
     *
     * @param row the row to set
     */
    void set(@NotNull Row row) {
        this.checkContentsSet();

        //noinspection ConstantConditions
        if (row == null) {
            throw new NullPointerException();
        }

        if (row.getContainer() != this) {
            throw new IllegalArgumentException("The value's container is not this object");
        }

        int rowIndex = row.getRowIdentifier().getRowPosition();
        if (this.contents[rowIndex] != null) {
            throw new IllegalArgumentException("RowBundle already had a row at " + row);
        }

        this.contents[rowIndex] = row;
    }
}
