/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Actually does the inserting into the db once references, insert order, etc have been figured out.
 */
@NotThreadSafe
final class InsertSqlAction extends ContextAware implements SqlAction<Integer> {
    @Nullable
    private final Value autoIdValue;
    @NotNull
    private final String sql;
    @NotNull
    private final LinkedHashSet<Map.Entry<ColumnIdentifier, String>> orderedRowData;

    /**
     * Use this ctor if there is an auto id value.
     *
     * @param context        context
     * @param sql            the sql string to prepare
     * @param orderedRowData the columns to set in the order used to prepare the sql statement
     * @param autoIdValue    the value that should be replaced with the inserted id
     */
    InsertSqlAction(@NotNull ContextStack context, @NotNull String sql,
            @NotNull LinkedHashSet<Map.Entry<ColumnIdentifier, String>> orderedRowData, @NotNull Value autoIdValue) {
        super(context);
        this.autoIdValue = autoIdValue;
        this.sql = sql;
        this.orderedRowData = orderedRowData;
    }

    /**
     * Use this ctor if there is no auto id value.
     *
     * @param context        context
     * @param sql            the sql string to prepare
     * @param orderedRowData the columns to set in the order used to prepare the sql statement
     */
    InsertSqlAction(@NotNull ContextStack context, @NotNull String sql,
            @NotNull LinkedHashSet<Map.Entry<ColumnIdentifier, String>> orderedRowData) {
        super(context);
        this.autoIdValue = null;
        this.sql = sql;
        this.orderedRowData = orderedRowData;
    }

    /**
     * @param conn the JDBC connection to use
     *
     * @return the last inserted id if there was an auto column, otherwise null
     *
     * @throws java.sql.SQLException on any sql error
     */
    @Override
    @Nullable
    public Integer doAction(Connection conn) throws SQLException {
        final PreparedStatement prepStmt = conn.prepareStatement(this.sql, Statement.RETURN_GENERATED_KEYS);
        Integer lastInsertedId = null;
        try {
            Iterator<Map.Entry<ColumnIdentifier, String>> columnIter = this.orderedRowData.iterator();
            int i = 1;
            while (columnIter.hasNext()) {
                Map.Entry<ColumnIdentifier, String> col = columnIter.next();
                String colValue = col.getValue();

                prepStmt.setString(i, colValue);
                i++;
            }

            prepStmt.executeUpdate();

            if (this.autoIdValue != null) {
                // we care about the generated key
                final ResultSet resultSet = prepStmt.getGeneratedKeys();
                try {
                    boolean hasGenKey = resultSet.next();
                    if (!hasGenKey) {
                        throw this.fail("An auto-increment value was requested," +
                                " but no such auto-increment column was" + " detected on the insert");
                    }

                    lastInsertedId = resultSet.getInt(1);

                    if (!resultSet.isLast()) {
                        throw this.fail("Got more than one generated key back from the insert");
                    }

                    resultSet.close();
                } finally {
                    try {
                        resultSet.close();
                    } catch (SQLException e) {
                        logger.error("Error while closing generated key result set", e);
                    }
                }
            }

            conn.commit();
            prepStmt.close();
        } finally {
            try {
                prepStmt.close();
            } catch (SQLException e) {
                logger.error("Error while closing prepared statement", e);
            }
        }
        return lastInsertedId;
    }
}