/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

/**
 * A single column value in a row
 */
@NotThreadSafe
public final class Value extends AbstractContainable<ColumnIdentifier, Row> {

    /**
     * the string contents of the value
     */
    private final String value;

    /**
     * Create a new value, intialized with the provided value string
     *
     * @param row   the row that this value is in
     * @param name  the column that this is the value for
     * @param value the string value
     */
    Value(@NotNull Row row, @NotNull ColumnIdentifier name, @NotNull String value) {
        super(name, row);

        this.value = value;
    }

    /**
     * @return the value as an int
     *
     * @throws NumberFormatException
     */
    public int asInt() {
        return Integer.parseInt(this.value);
    }

    /**
     * @return the value as a long
     *
     * @throws NumberFormatException
     */
    public long asLong() {
        return Long.parseLong(this.value);
    }

    /**
     * @return the value as a float
     *
     * @throws NumberFormatException
     */
    public float asFloat() {
        return Float.parseFloat(this.value);
    }

    /**
     * @return the value as a double
     *
     * @throws NumberFormatException
     */
    public double asDouble() {
        return Double.parseDouble(this.value);
    }

    /**
     * @return the value as a boolean. This is boolean true if the string is not null and is case-insensitive equal to
     *         "true".
     */
    @SuppressWarnings({"BooleanMethodNameMustStartWithQuestion"})
    public boolean asBoolean() {
        return Boolean.parseBoolean(this.value);
    }

    /**
     * @return the value as a string
     */
    public String asString() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Value[" + this.getContainer().toString() + "." + this.getIdentifier() + "=" + this.value + "]";
    }
}
