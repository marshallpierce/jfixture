/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

/**
 * Checks that column references point to columns that exist
 */
@NotThreadSafe
final class ColumnReferenceResolver extends ContextAware {

    /**
     * the repos to check refs against
     */
    private final FixtureGroupRepository repos;

    ColumnReferenceResolver(FixtureGroupRepository repos) {
        this.repos = repos;
    }

    ColumnReferenceResolver(FixtureGroupRepository repos, ContextStack context) {
        super(context);
        this.repos = repos;
    }

    /**
     * It might seem that you'd want to get a Column out of this, not a Row, but Rows are the most convenient unit to
     * pass around since they're the smallest unit that can be inserted.
     *
     * @param ref the ref to check
     *
     * @return the row that contains the column referred to by ref. The row is guaranteed to contain the column.
     *
     * @throws FixtureException if the reference cannot be resolved
     */
    @SuppressWarnings({"ConstantConditions"})
    @NotNull
    Value resolve(ColumnReference ref) {
        try {
            this.pushContext("ref " + ref);
            try {
                DbIdentifier dbName = ref.getDbIdentifier();
                this.pushContext("dbName " + dbName);

                FixtureGroup group = this.repos.getOrNull(dbName);
                if (group == null) {
                    throw this.fail("Couldn't find the group for the db name");
                }

                try {
                    FixtureIdentifier fixtureName = ref.getFixtureIdentifier();
                    this.pushContext("fixture name " + fixtureName);

                    Fixture fixture = group.getOrNull(fixtureName);
                    if (fixture == null) {
                        throw this.fail("Couldn't find the fixture");
                    }

                    TableIdentifier tableIdentifier = ref.getTableName();
                    this.pushContext("table name " + tableIdentifier);

                    try {
                        RowBundle<?> rowBundle = fixture.getOrNull(tableIdentifier);
                        if (rowBundle == null) {
                            throw this.fail("Couldn't find the rowBundle");
                        }

                        RowIdentifierType tableType = rowBundle.getType();
                        RowIdentifierType rowIdentifierType = ref.getRowIdentifierType();

                        if (rowIdentifierType != tableType) {
                            throw this.fail("row ref was of type " + rowIdentifierType +
                                    " but rowBundle was of incompatible type " + tableType);
                        }

                        this.pushContext("row id " + ref.getRowIdentifier());
                        try {
                            Row row = rowBundle.getOrNull(ref.getRowIdentifier());

                            if (row == null) {
                                throw this.fail("Couldn't find row");
                            }

                            ColumnIdentifier colName = ref.getColumnIdentifier();

                            this.pushContext("column name " + colName);
                            try {
                                Value v = row.getOrNull(colName);
                                if (v == null) {
                                    throw this.fail("Couldn't find column");
                                }

                                // success: have verified that the dependency can be satisfied
                                return v;
                            } finally {
                                // col name
                                this.popContext();
                            }
                        } finally {
                            // row identifier
                            this.popContext();
                        }
                    } finally {
                        // table namef
                        this.popContext();
                    }
                } finally {
                    // fixture name
                    this.popContext();
                }
            } finally {
                // db name
                this.popContext();
            }
        } finally {
            // ref string
            this.popContext();
        }
    }
}
