/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * A Container is something that contains other objects, like how a Row contains Values, each of which has a Name.
 *
 * @param <N> the identifier type used in this container
 * @param <V> the value type in the container
 */
interface Container<N, V extends Containable<N, ?>> {
    /**
     * @param identifier the identifier
     *
     * @return the value for that identifier
     *
     * @throws FixtureException if no value exists for that identifier
     */
    @NotNull
    V get(@NotNull N identifier);

    /**
     * Useful when you want to be able to do error recovery if a value does not exist.
     *
     * @param identifier the identifier
     *
     * @return the value for that identifier. or null if no such value exists
     */
    @Nullable
    V getOrNull(@NotNull N identifier);

    @NotNull
    Set<N> keySet();

    int size();

    @NotNull
    Set<Map.Entry<N, V>> entrySet();

    @NotNull
    Collection<V> values();
}
