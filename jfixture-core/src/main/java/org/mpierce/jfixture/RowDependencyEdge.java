/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.Immutable;
import org.jetbrains.annotations.NotNull;

/**
 * A directed edge in the dependency graph. equals() and hashCode() have been overridden to compare the underlying start
 * and end nodes with pointer equality.
 */
@Immutable
final class RowDependencyEdge {

    /**
     * The row that needs the other row's data
     */
    @NotNull
    private final RowDependencyNode start;

    /**
     * The row that has the data the other row needs
     */
    @NotNull
    private final RowDependencyNode end;

    /**
     * @param start the row that needs another row's data. Can't be null.
     * @param end   the row providing the data that the other row needs. Can't be null. Can't be the same as start.
     *
     * @throws NullPointerException
     */
    @SuppressWarnings({"ConstantConditions"})
    RowDependencyEdge(@NotNull RowDependencyNode start, @NotNull RowDependencyNode end) {
        if (start == null) {
            throw new NullPointerException();
        }
        if (end == null) {
            throw new NullPointerException();
        }
        if (end == start) {
            throw new IllegalArgumentException("The start and end cannot be the same");
        }

        this.start = start;
        this.end = end;
    }

    /**
     * @return the node that depends on another node. Never null.
     */
    @NotNull
    RowDependencyNode getStart() {
        return this.start;
    }

    /**
     * @return the node that is depended upon. Never null.
     */
    @NotNull
    RowDependencyNode getEnd() {
        return this.end;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.end.hashCode();
        result = prime * result + this.start.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof RowDependencyEdge)) {
            return false;
        }
        RowDependencyEdge other = (RowDependencyEdge) obj;
        if (!this.end.equals(other.end)) {
            return false;
        }
        //noinspection RedundantIfStatement
        if (!this.start.equals(other.start)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RowDependencyEdge{" + "start=" + start.getRow() + ", end=" + end.getRow() + '}';
    }
}
