/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.Immutable;
import org.jetbrains.annotations.NotNull;

/**
 * A reference to another column. References may be expressed in the fixture specification in several ways.
 *
 * someCol: <<fixtureName.tableName.columnName>> This is used when the table has only one row.
 *
 * someCol: <<fixtureName.tableName.[row index number].columnName>> This is used when the table has several rows
 * formatted as a list.
 *
 * someCol: <<fixtureName.tableName.[row nickname].columnName>> This is used when the table has several rows formatted
 * as a map where each row has a name.
 *
 * However, we can't tell the difference at this point in the process between a row name that happens to be numeric and
 * an actual row index, so we treat them both as strings for now.
 */
@Immutable
final class ColumnReference {

    @NotNull
    private final DbIdentifier dbIdentifier;
    @NotNull
    private final FixtureIdentifier fixtureIdentifier;
    @NotNull
    private final TableIdentifier tableIdentifier;

    @NotNull
    private final ColumnIdentifier columnIdentifier;
    @NotNull
    private final RowIdentifierType refType;

    @NotNull
    private final RowIdentifier rowIdentifier;

    private ColumnReference(@NotNull DbIdentifier dbIdentifier, @NotNull FixtureIdentifier fixtureIdentifier,
            @NotNull TableIdentifier tableIdentifier, @NotNull String rowName,
            @NotNull ColumnIdentifier columnIdentifier) {
        this.dbIdentifier = dbIdentifier;
        this.fixtureIdentifier = fixtureIdentifier;
        this.tableIdentifier = tableIdentifier;
        this.columnIdentifier = columnIdentifier;
        this.refType = RowIdentifierType.NAMED_ROW;

        this.rowIdentifier = new NamedRowIdentifier(rowName);
    }

    private ColumnReference(@NotNull DbIdentifier dbIdentifier, @NotNull FixtureIdentifier fixtureIdentifier,
            @NotNull TableIdentifier tableIdentifier, int rowIndex, @NotNull ColumnIdentifier columnIdentifier) {
        this.dbIdentifier = dbIdentifier;
        this.fixtureIdentifier = fixtureIdentifier;
        this.tableIdentifier = tableIdentifier;
        this.columnIdentifier = columnIdentifier;
        this.refType = RowIdentifierType.INDEXED_ROW;

        this.rowIdentifier = new IndexedRowIdentifier(rowIndex);
    }

    private ColumnReference(@NotNull DbIdentifier dbIdentifier, @NotNull FixtureIdentifier fixtureIdentifier,
            @NotNull TableIdentifier tableIdentifier, @NotNull ColumnIdentifier columnIdentifier) {
        this.dbIdentifier = dbIdentifier;
        this.fixtureIdentifier = fixtureIdentifier;
        this.tableIdentifier = tableIdentifier;
        this.columnIdentifier = columnIdentifier;
        this.refType = RowIdentifierType.ANONYMOUS_ROW;

        this.rowIdentifier = new AnonymousRowIdentifier();
    }

    /**
     * A reference to a row that is the only row in its table for a given fixture.
     *
     * @param dbName          the db name
     * @param fixtureName     the fixture name
     * @param tableIdentifier the table name
     * @param columnName      the column name
     *
     * @return a row ref
     */
    static ColumnReference getAnonymousRowRef(DbIdentifier dbName, FixtureIdentifier fixtureName,
            TableIdentifier tableIdentifier, ColumnIdentifier columnName) {
        return new ColumnReference(dbName, fixtureName, tableIdentifier, columnName);
    }

    /**
     * A reference to a row that is identified by a label among the rows in the table.
     *
     * @param dbName          the db name
     * @param fixtureName     the fixture name
     * @param tableIdentifier the table name
     * @param rowIndex        the row index
     * @param columnName      the column name
     *
     * @return a row ref
     */
    static ColumnReference getIndexedRowRef(DbIdentifier dbName, FixtureIdentifier fixtureName,
            TableIdentifier tableIdentifier, int rowIndex, ColumnIdentifier columnName) {
        return new ColumnReference(dbName, fixtureName, tableIdentifier, rowIndex, columnName);
    }

    /**
     * A reference to a row that is identified by a label among the rows in the table.
     *
     * @param dbName          the db name
     * @param fixtureName     the fixture name
     * @param tableIdentifier the table name
     * @param rowName         the row name
     * @param columnName      the column name
     *
     * @return a row ref
     */
    static ColumnReference getNamedRowRef(DbIdentifier dbName, FixtureIdentifier fixtureName,
            TableIdentifier tableIdentifier, String rowName, ColumnIdentifier columnName) {
        return new ColumnReference(dbName, fixtureName, tableIdentifier, rowName, columnName);
    }

    @NotNull
    RowIdentifierType getRowIdentifierType() {
        return this.refType;
    }

    @NotNull
    DbIdentifier getDbIdentifier() {
        return this.dbIdentifier;
    }

    @NotNull
    FixtureIdentifier getFixtureIdentifier() {
        return this.fixtureIdentifier;
    }

    @NotNull
    TableIdentifier getTableName() {
        return this.tableIdentifier;
    }

    @NotNull
    RowIdentifier getRowIdentifier() {
        return this.rowIdentifier;
    }

    @NotNull
    ColumnIdentifier getColumnIdentifier() {
        return this.columnIdentifier;
    }

    @Override
    public String toString() {
        String buf = this.dbIdentifier + "/" + this.fixtureIdentifier + "." + this.tableIdentifier;

        switch (this.refType) {
            case ANONYMOUS_ROW:
                break;
            case INDEXED_ROW:
                buf += "[" + this.rowIdentifier.getRowPosition() + "]";
                break;
            case NAMED_ROW:
                buf += "{" + this.rowIdentifier.getRowName() + "}";
                break;
        }

        buf += "." + this.columnIdentifier;

        return buf;
    }
}
