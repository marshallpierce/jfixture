/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Reader;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * External interface for loading fixtures.
 */
@NotThreadSafe
public final class FixtureController {

    /**
     * map of db names to DataSource objects
     */
    private final ConnectionProviderRepository connectionProviderRepos = new ConnectionProviderRepository();

    /**
     * map of db names to FixtureGroup objects
     */
    private final FixtureGroupRepository fixtureGroupReposFromYaml = new FixtureGroupRepository();

    /**
     * set during finish()
     */
    private Loader loader;

    /**
     * @param dbName     Arbitrary name that you use to refer to this db.
     * @param dataSource The DataSource to insert into.
     */
    public void addDb(@NotNull String dbName, @NotNull DataSource dataSource) {
        this.addDb(dbName, new DataSourceConnectionProvider(dataSource));
    }

    /**
     * @param dbName             Arbitrary name that you use to refer to this db.
     * @param connectionProvider the connection provider
     */
    public void addDb(@NotNull String dbName, @NotNull ConnectionProvider connectionProvider) {
        this.checkNotFinished();
        this.connectionProviderRepos
                .add(new ConnectionProviderWrapper(new DbIdentifier(dbName), this.connectionProviderRepos,
                        connectionProvider));
    }

    /**
     * @param dbName db name
     * @param file   File containing yaml
     *
     * @throws FileNotFoundException if the file isn't found
     */
    public void setDbContents(@NotNull String dbName, @NotNull File file) throws FileNotFoundException {
        addYamlStructure(dbName, YamlData.getYamlData(file));
    }

    /**
     * @param dbName db name to add data to
     * @param stream InputStream containing yaml data
     */
    public void setDbContents(@NotNull String dbName, @NotNull InputStream stream) {
        addYamlStructure(dbName, YamlData.getYamlData(stream));
    }

    /**
     * @param dbName db name to add data to
     * @param reader Reader containing yaml data
     */
    public void setDbContents(@NotNull String dbName, @NotNull Reader reader) {
        addYamlStructure(dbName, YamlData.getYamlData(reader));
    }

    /**
     * Actually inserts into the database. This should be called once you are finished adding databases and setting
     * their contents.
     *
     * @throws FixtureException
     */
    public void finish() {
        this.checkNotFinished();
        ReferenceFinder finder = new ReferenceFinder();
        finder.traverse(this.fixtureGroupReposFromYaml);
        Set<RowLink> rowLinks = finder.getRefs();
        Map<Row, Value> autoIdValues = finder.getAutoIdValues();
        Map<Value, ColumnReference> valuesWithRefs = finder.getValuesWithRefs();

        RowDependencyGraph graph = RowDependencyGraphAssembler.assemble(rowLinks, this.fixtureGroupReposFromYaml);
        List<Row> rowsToInsert = graph.getRowsInInsertionOrder();
        RowAggregator aggregator = new RowAggregator();
        aggregator.traverse(this.fixtureGroupReposFromYaml);
        Set<Row> allRows = new HashSet<Row>(aggregator.getRows());
        allRows.removeAll(rowsToInsert);
        rowsToInsert.addAll(allRows);

        this.loader = new Loader(rowsToInsert, autoIdValues, valuesWithRefs, this.connectionProviderRepos,
                this.fixtureGroupReposFromYaml);
        loader.insertRows();
    }

    /**
     * Empties every table that was populated via the yaml data.
     */
    public void clearTables() {
        this.checkFinished();
        this.loader.clearTables();
    }

    /**
     * Get a value from a named row. These are named rows (yaml map syntax):
     * <pre>
     *  fixtName:
     *      tableName:
     *          rowName1:
     *              col1: value1
     *          rowName2:
     *              col1: value2
     * </pre>
     *
     * @return the value
     */
    @NotNull
    @SuppressWarnings({"JavaDoc"})
    public Value get(@NotNull String dbName, @NotNull String fixtureName, @NotNull String tableName,
            @NotNull String rowName, @NotNull String columnName) {
        return this.get(ColumnReference.getNamedRowRef(new DbIdentifier(dbName), new FixtureIdentifier(fixtureName),
                new TableIdentifier(tableName), rowName, new ColumnIdentifier(columnName)));
    }

    /**
     * Get a value from an indexed row. These are indexed rows (yaml list syntax):
     * <pre>
     *  fixtName:
     *      tableName:
     *          -
     *              col1: value1
     *          -
     *              col1: value2
     * </pre>
     *
     * @return the value
     */
    @NotNull
    @SuppressWarnings({"JavaDoc"})
    public Value get(@NotNull String dbName, @NotNull String fixtureName, @NotNull String tableName, int rowIndex,
            @NotNull String columnName) {
        return this.get(ColumnReference.getIndexedRowRef(new DbIdentifier(dbName), new FixtureIdentifier(fixtureName),
                new TableIdentifier(tableName), rowIndex, new ColumnIdentifier(columnName)));
    }

    /**
     * Get a value from an anonymous row. This is an anonymous row:
     * <pre>
     *  fixtName:
     *      tableName:
     *          col1: value1
     * </pre>
     *
     * @return the value
     */
    @SuppressWarnings({"JavaDoc"})
    @NotNull
    public Value get(@NotNull String dbName, @NotNull String fixtureName, @NotNull String tableName,
            @NotNull String columnName) {
        return this.get(ColumnReference.getAnonymousRowRef(new DbIdentifier(dbName), new FixtureIdentifier(fixtureName),
                new TableIdentifier(tableName), new ColumnIdentifier(columnName)));
    }

    private void addYamlStructure(String dbName, Object yamlStructure) {
        this.checkNotFinished();
        FixtureGroup fixtureGroup = new FixtureExtractor()
                .extractUnresolvedFixtures(this.fixtureGroupReposFromYaml, new DbIdentifier(dbName), yamlStructure);
        this.fixtureGroupReposFromYaml.add(fixtureGroup);
    }

    @NotNull
    private Value get(ColumnReference ref) {
        this.checkFinished();
        return this.loader.getResolver().resolve(ref);
    }

    @SuppressWarnings({"NonBooleanMethodNameMayNotStartWithQuestion"})
    private void checkFinished() {
        if (this.loader == null) {
            throw new IllegalStateException("You must call finish() first");
        }
    }

    @SuppressWarnings({"NonBooleanMethodNameMayNotStartWithQuestion"})
    private void checkNotFinished() {
        if (this.loader != null) {
            throw new IllegalStateException("You already called finish()");
        }
    }
}
