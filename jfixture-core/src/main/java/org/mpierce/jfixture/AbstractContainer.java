/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * An implementation of Container providing all the basic methods. Intended to be subclassed.
 *
 * @param <N> the name type used in this container
 * @param <V> the value type held in this container
 */
abstract class AbstractContainer<N, V extends Containable<N, ?>> implements MutableContainer<N, V> {
    /**
     * map of names to values. No null values.
     */
    @NotNull
    private final Map<N, V> map;

    AbstractContainer() {
        this.map = new HashMap<N, V>();
    }

    @Override
    @NotNull
    public final V get(@NotNull N identifier) {
        if (!this.map.containsKey(identifier)) {
            throw new FixtureException("Unkown identifier <" + identifier + ">");
        }
        return this.map.get(identifier);
    }

    @Override
    @Nullable
    public final V getOrNull(@NotNull N identifier) {
        return this.map.get(identifier);
    }

    @NotNull
    @Override
    public final Set<N> keySet() {
        return Collections.unmodifiableSet(this.map.keySet());
    }

    /**
     * @return the size of the map
     */
    @Override
    public final int size() {
        return this.map.size();
    }

    @NotNull
    @Override
    public final Collection<V> values() {
        return Collections.unmodifiableCollection(this.map.values());
    }

    @Override
    public final void add(@NotNull V value) {
        //noinspection ConstantConditions
        if (value == null) {
            throw new IllegalArgumentException("Null values are not allowed");
        }

        N identifier = value.getIdentifier();

        if (this.map.containsKey(identifier)) {
            throw new IllegalArgumentException("A value already exists for key " + identifier);
        }

        if (value.getContainer() != this) {
            throw new IllegalArgumentException("The value's container is not this object");
        }

        this.map.put(identifier, value);
    }

    @NotNull
    @Override
    public final Set<Map.Entry<N, V>> entrySet() {
        return Collections.unmodifiableSet(this.map.entrySet());
    }
}
