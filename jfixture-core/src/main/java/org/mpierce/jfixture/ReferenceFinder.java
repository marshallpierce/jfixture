/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Used to look for column references.
 */
@NotThreadSafe
final class ReferenceFinder extends FixtureVisitor {

    private static final String REF_END = Pattern.quote("}");
    private static final String REF_START = Pattern.quote("${");

    private static final Pattern FIXTURE_AND_TABLE_PATTERN = Pattern.compile("(\\w+)\\.(\\w+)");
    private static final Pattern ANCHORED_FIXTURE_AND_TABLE_PATTERN = Pattern.compile("^" + FIXTURE_AND_TABLE_PATTERN);

    private static final Pattern NAMED_ROW_REF_PATTERN =
            Pattern.compile(FIXTURE_AND_TABLE_PATTERN + "\\{(\\w+)\\}\\.(\\w+)");
    private static final Pattern ANCHORED_NAMED_ROW_REF_PATTERN = Pattern.compile("^" + NAMED_ROW_REF_PATTERN + "$");

    private static final Pattern INDEXED_ROW_REF_PATTERN =
            Pattern.compile(FIXTURE_AND_TABLE_PATTERN + "\\[([0-9]+)\\]\\.(\\w+)");
    private static final Pattern ANCHORED_INDEXED_ROW_REF_PATTERN =
            Pattern.compile("^" + INDEXED_ROW_REF_PATTERN + "$");

    private static final Pattern ANONYMOUS_ROW_REF_PATTERN = Pattern.compile(FIXTURE_AND_TABLE_PATTERN + "\\.(\\w+)");
    private static final Pattern ANCHORED_ANONYMOUS_ROW_REF_PATTERN =
            Pattern.compile("^" + ANONYMOUS_ROW_REF_PATTERN + "$");

    /**
     * the guts of the pattern to match a fixture.table.(optional row id).column reference
     */
    private static final String GENERIC_REF_PATTERN =
            "(?:(?:" + NAMED_ROW_REF_PATTERN + ")|(?:" + INDEXED_ROW_REF_PATTERN + ")|(?:" + ANONYMOUS_ROW_REF_PATTERN +
                    "))";

    /**
     * pattern that matches references that are scoped to be from a different database
     */
    private static final Pattern SCOPED_REF_PATTERN = Pattern.compile("^(\\w+)/(" + GENERIC_REF_PATTERN + ")$");

    /**
     * matches the fixture.table..column reference
     */
    private static final Pattern UNSCOPED_REF_PATTERN = Pattern.compile("^" + GENERIC_REF_PATTERN + "$");
    /**
     * the pattern to check values against to see if they are special values (auto-increment keys or references to other
     * fixtures or tables)
     */
    private static final Pattern REFERENCE_VALUE_PATTERN = Pattern.compile("^" + REF_START + "(.*)" + REF_END + "$");

    /**
     * The magic fill-this-in-with-generated-primary-key ref
     */
    private static final String AUTO_TOKEN = "autoId";

    /**
     * references that have been found
     */
    private final Set<RowLink> refs = new HashSet<RowLink>();

    /**
     * If a row has a value
     */
    private final Map<Row, Value> autoIdValues = new HashMap<Row, Value>();
    /**
     * values that contained references mapped to the resulting references
     */
    private final Map<Value, ColumnReference> valuesWithRefs = new HashMap<Value, ColumnReference>();
    private static final String SELF_REFERENCE_FIXTURE_NAME = "this";

    Map<Value, ColumnReference> getValuesWithRefs() {
        return Collections.unmodifiableMap(this.valuesWithRefs);
    }

    Map<Row, Value> getAutoIdValues() {
        return Collections.unmodifiableMap(this.autoIdValues);
    }

    /**
     * @param dbName the database that this row is in
     * @param row    the row that the value is in
     * @param value  the value to look for references in
     *
     * @return a reference if one was found, otherwise null
     *
     * @throws FixtureException on parse errors
     */
    @Nullable
    private ColumnReference getColumnReference(@NotNull DbIdentifier dbName, @NotNull Row row, @NotNull Value value) {
        Matcher valueMatcher = REFERENCE_VALUE_PATTERN.matcher(value.asString());

        // if the value str is a reference, this will be the
        // contents of the reference

        if (!valueMatcher.matches()) {
            // didn't find a special value, so can't be a row ref

            return null;
        }

        String refContents = valueMatcher.group(1);

        if (refContents.equals(AUTO_TOKEN)) {
            // auto isn't a dependency, but we do want to keep track of it for later use
            Value oldValue = this.autoIdValues.put(row, value);
            if (oldValue != null) {
                throw this.fail("Two auto-id values in the same row detected");
            }
            return null;
        }

        this.pushContext("ref=" + refContents);
        try {
            Matcher dbScopedRefMatcher = SCOPED_REF_PATTERN.matcher(refContents);
            Matcher unscopedRefMatcher = UNSCOPED_REF_PATTERN.matcher(refContents);

            DbIdentifier refDbName = dbName;

            String refStr;
            if (dbScopedRefMatcher.matches()) {
                // change the db name and the ref matcher to use
                refDbName = new DbIdentifier(dbScopedRefMatcher.group(1));
                refStr = dbScopedRefMatcher.group(2);
            } else if (unscopedRefMatcher.matches()) {
                refStr = refContents;
            } else {
                // should this simply return null?
                throw this.fail("Couldn't parse ref");
            }

            ColumnReference reference =
                    extractRef(refDbName, refStr, row.getContainer().getContainer().getIdentifier());
            this.valuesWithRefs.put(value, reference);
            return reference;
        } finally {
            this.popContext();
        }
    }

    /**
     * @param refDbName          the db that the ref is scoped to
     * @param refStr             the complete ref string
     * @param currentFixtureName the name of the fixture this ref is in
     *
     * @return the ref object
     */
    @NotNull
    private ColumnReference extractRef(DbIdentifier refDbName, String refStr, FixtureIdentifier currentFixtureName) {
        Matcher fixtureAndTableMatcher = ANCHORED_FIXTURE_AND_TABLE_PATTERN.matcher(refStr);

        if (!fixtureAndTableMatcher.find()) {
            throw this.fail("Couldn't extract fixture and table from ref");
        }

        FixtureIdentifier fixtureName = new FixtureIdentifier(fixtureAndTableMatcher.group(1));

        if (SELF_REFERENCE_FIXTURE_NAME.equals(fixtureName.toString())) {
            fixtureName = currentFixtureName;
        }

        TableIdentifier tableIdentifier = new TableIdentifier(fixtureAndTableMatcher.group(2));
        ColumnIdentifier colName;

        Matcher namedRefMatcher = ANCHORED_NAMED_ROW_REF_PATTERN.matcher(refStr);
        Matcher indexedRefMatcher = ANCHORED_INDEXED_ROW_REF_PATTERN.matcher(refStr);
        Matcher anonRefMatcher = ANCHORED_ANONYMOUS_ROW_REF_PATTERN.matcher(refStr);

        if (namedRefMatcher.matches()) {
            String rowName = namedRefMatcher.group(3);
            colName = new ColumnIdentifier(namedRefMatcher.group(4));
            return ColumnReference.getNamedRowRef(refDbName, fixtureName, tableIdentifier, rowName, colName);
        } else if (indexedRefMatcher.matches()) {
            String rowIndexStr = indexedRefMatcher.group(3);
            colName = new ColumnIdentifier(indexedRefMatcher.group(4));
            return ColumnReference
                    .getIndexedRowRef(refDbName, fixtureName, tableIdentifier, Integer.parseInt(rowIndexStr), colName);
        } else if (anonRefMatcher.matches()) {
            colName = new ColumnIdentifier(anonRefMatcher.group(3));
            return ColumnReference.getAnonymousRowRef(refDbName, fixtureName, tableIdentifier, colName);
        }

        throw this.fail("Couldn't detect ref type");
    }

    @Override
    void onRow(@NotNull Row row) {
    }

    Set<RowLink> getRefs() {
        return Collections.unmodifiableSet(this.refs);
    }

    @Override
    void onValue(@NotNull DbIdentifier dbName, @NotNull Row row, @NotNull Value value) {
        ColumnReference ref = getColumnReference(dbName, row, value);

        if (ref == null) {
            return;
        }

        this.refs.add(new RowLink(row, ref));
    }
}
