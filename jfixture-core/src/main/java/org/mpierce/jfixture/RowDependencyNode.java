/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;

import java.util.HashSet;
import java.util.Set;

/**
 * A node in the dependency graph.
 */
@NotThreadSafe
final class RowDependencyNode {

    /**
     * edges from rows that depend on this row
     */
    private final Set<RowDependencyEdge> incoming = new HashSet<RowDependencyEdge>();

    /**
     * edges to rows that this row depends on
     */
    private final Set<RowDependencyEdge> outgoing = new HashSet<RowDependencyEdge>();

    /**
     * the row that this node represents
     */
    private final Row row;

    /**
     * @param row the row that this node represents
     */
    RowDependencyNode(Row row) {
        this.row = row;
    }

    /**
     * @return the row
     */
    Row getRow() {
        return this.row;
    }

    /**
     * @param edge an edge from some other node to this node
     */
    void addIncoming(RowDependencyEdge edge) {
        if (edge.getEnd() != this) {
            throw new IllegalArgumentException("An edge was added as incoming that did not end at this node");
        }

        if (!this.incoming.add(edge)) {
            throw new IllegalStateException("An incoming edge was added twice");
        }
    }

    /**
     * @param edge an edge from this node to some other node
     */
    void addOutgoing(RowDependencyEdge edge) {
        if (edge.getStart() != this) {
            throw new IllegalArgumentException("An edge was added as outgoing that did not start at this node");
        }

        if (!this.outgoing.add(edge)) {
            throw new IllegalStateException("An outgoing edge was added twice");
        }
    }

    /**
     * @return all nodes that this node depends on
     */
    Set<RowDependencyNode> getDependencies() {
        Set<RowDependencyNode> dependencies = new HashSet<RowDependencyNode>();

        for (RowDependencyEdge edge : this.outgoing) {
            dependencies.add(edge.getEnd());
        }

        return dependencies;
    }

    @Override
    public String toString() {
        return "RowDependencyNode{" + "row=" + this.row + ", incoming=" + this.incoming + ", outgoing=" +
                this.outgoing + '}';
    }
}
