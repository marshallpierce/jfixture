/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;

/**
 * A single row to be loaded into the db.
 *
 * It's kind of weird to have Row have a Name, since there are several ways Rows can be referred to...
 */
@NotThreadSafe
final class Row extends AbstractContainableContainer<RowIdentifier, RowBundle<?>, ColumnIdentifier, Value> {

    /**
     * Create a new row and initializes it with the specified values. Make sure to call setTable after creating its
     * parent rowBundle.
     *
     * @param rowBundle  the rowBundle this row is in
     * @param identifier the identifier that describes how the row is related to its parent rowBundle
     */
    Row(RowBundle<?> rowBundle, RowIdentifier identifier) {
        super(rowBundle, identifier);
    }

    /**
     * @return the identifier
     */
    RowIdentifier getRowIdentifier() {
        return this.getIdentifier();
    }

    @Override
    public String toString() {
        RowBundle<?> rowBundle = this.getContainer();
        Fixture fixture = rowBundle.getContainer();
        FixtureGroup group = fixture.getContainer();


        String locator = group.getIdentifier() + "/" + fixture.getIdentifier() + "." + rowBundle.getIdentifier();

        switch (this.getRowIdentifier().getType()) {
            case ANONYMOUS_ROW:
                locator += "." + this.getRowIdentifier().toString();
                break;
            case INDEXED_ROW:
                locator += "[" + this.getRowIdentifier().getRowPosition() + "]";
                break;
            case NAMED_ROW:
                locator += "{" + this.getRowIdentifier().getRowName() + "}";
                break;
        }

        return locator;
    }
}
