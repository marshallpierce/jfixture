/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A RowBundle that contains rows identified by string names.
 */
@NotThreadSafe
final class RowMapBundle extends RowBundle<Map<String, Row>> {

    /**
     * @param fixture    the fixture that contains this object
     * @param identifier the table name
     */
    RowMapBundle(Fixture fixture, TableIdentifier identifier) {
        super(fixture, identifier, RowIdentifierType.NAMED_ROW);
    }

    @SuppressWarnings({"RefusedBequest"})
    @Override
    Row getRowWithName(String rowName) {
        return this.contents.get(rowName);
    }

    @Override
    Set<Row> getRows() {
        return new HashSet<Row>(this.contents.values());
    }

    @Override
    int size() {
        return this.contents.size();
    }

    /**
     * This is only to be used when the table must be added to incrementally (during construction of the resolved
     * fixture structure)
     *
     * @param row the row to set
     */
    void set(@NotNull Row row) {
        this.checkContentsSet();

        //noinspection ConstantConditions
        if (row == null) {
            throw new IllegalArgumentException("Null values are not allowed");
        }

        String rowName = row.getRowIdentifier().getRowName();
        if (this.contents.containsKey(rowName)) {
            throw new IllegalArgumentException("A value already exists for key " + rowName);
        }

        if (row.getContainer() != this) {
            throw new IllegalArgumentException("The value's container is not this object");
        }

        this.contents.put(rowName, row);
    }
}
