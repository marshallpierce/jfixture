/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import net.jcip.annotations.NotThreadSafe;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A convenience base class to provide context utility methods
 */
@NotThreadSafe
abstract class ContextAware {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * debug context. Can be overridden if a non-empty stack is to be used.
     */
    final ContextStack context;

    ContextAware() {
        // start with empty context
        this(new ContextStack());
    }

    ContextAware(@NotNull ContextStack context) {
        this.context = context;
    }

    /**
     * Assemble an informative exception message, append the supplied msg parameter, and use it to create a
     * FixtureException that the caller will then throw.
     *
     * @param msg exception message
     *
     * @return the exception
     */
    final FixtureException fail(@NotNull String msg) {
        return ContextUtil.generateContextException(this.context, msg);
    }

    /**
     * Assemble an informative exception message, append the supplied msg parameter, and use it to create a
     * FixtureException with the specified cause.
     *
     * @param msg   exception message
     * @param cause cause exception
     *
     * @return the exception
     */
    final FixtureException fail(@NotNull String msg, @NotNull Throwable cause) {
        return ContextUtil.generateContextException(this.context, msg, cause);
    }

    /**
     * Push the message onto the context stack.
     *
     * @param msg context message
     */
    final void pushContext(@NotNull String msg) {
        this.logger.trace("Entering context " + msg);
        this.context.push(msg);
    }

    /**
     * Pop the most recent message from the context stack.
     */
    final void popContext() {
        this.logger.trace("Exiting context " + this.context.pop());
    }
}
