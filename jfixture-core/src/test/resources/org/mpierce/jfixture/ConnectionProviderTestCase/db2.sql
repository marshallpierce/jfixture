SET IGNORECASE FALSE;

CREATE TABLE IF NOT EXISTS address (
    id IDENTITY,
    zip INT,
    cityId INT,
    street VARCHAR,
    userId INT
);

CREATE TABLE IF NOT EXISTS city (
    id IDENTITY,
    name INT,
    stateId INT
);

CREATE TABLE IF NOT EXISTS state (
    id IDENTITY,
    name INT
);