/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import com.mchange.v2.c3p0.DataSources;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public abstract class ConnectionProviderTestCase {
    @SuppressWarnings({"StaticNonFinalField"})
    protected static DataSource ds1 = null;
    @SuppressWarnings({"StaticNonFinalField"})
    protected static DataSource ds2 = null;

    protected static final String DB1_URL = "jdbc:h2:mem:db1";
    protected static final String DB2_URL = "jdbc:h2:mem:db2";
    protected static final String DRIVER_CLASS = "org.h2.Driver";

    protected FixtureController fixtureController;
    private static final String PREFIX = "ConnectionProviderTestCase";

    @BeforeClass
    public static void setUpClass() throws ClassNotFoundException, SQLException, IOException {
        Class.forName(DRIVER_CLASS);

        ds1 = DataSources.pooledDataSource(DataSources.unpooledDataSource(DB1_URL));
        ds2 = DataSources.pooledDataSource(DataSources.unpooledDataSource(DB2_URL));
    }

    @Before
    public void setUp() throws IOException, SQLException {
        execute(ds1, getPathAsString("db1.sql"));
        execute(ds2, getPathAsString("db2.sql"));
        this.fixtureController = setUpFixtureController();
    }

    /**
     * Implement this to set up the fixture controller with two connection providers registered as "db1" and "db2" that
     * point to the databases at urls DB1_URL and DB2_URL.
     *
     * @return the configured FixtureController
     */
    protected abstract FixtureController setUpFixtureController();

    @After
    public void tearDown() throws SQLException {
        execute(ds1, "DELETE FROM user");
        execute(ds1, "DELETE FROM unused");
    }

    @Test
    public void testMinimalYaml() throws FileNotFoundException, SQLException {
        fixtureController.setDbContents("db1", getStream(PREFIX, "db1-minimal.yaml"));
        fixtureController.finish();

        assertEquals("joe", fixtureController.get("db1", "fx1", "user", "joe", "name").asString());
        int joeId = fixtureController.get("db1", "fx1", "user", "joe", "id").asInt();

        assertEquals("joe", getOne(ds1, "SELECT name FROM user WHERE id = " + joeId));
        assertNull(getOne(ds1, "SELECT otherId FROM user WHERE id = " + joeId));
    }

    @Test
    public void testMinimalYamlViaReader() throws FileNotFoundException, SQLException {
        fixtureController.setDbContents("db1", new InputStreamReader(getStream(PREFIX, "db1-minimal.yaml")));
        fixtureController.finish();

        assertEquals("joe", fixtureController.get("db1", "fx1", "user", "joe", "name").asString());
        int joeId = fixtureController.get("db1", "fx1", "user", "joe", "id").asInt();

        assertEquals("joe", getOne(ds1, "SELECT name FROM user WHERE id = " + joeId));
        assertNull(getOne(ds1, "SELECT otherId FROM user WHERE id = " + joeId));
    }

    @Test
    public void testClearTablesDoesntTouchUnusedTables() throws SQLException {
        execute(ds1, "INSERT INTO unused (foo) VALUES ('asdf')");

        assertEquals("1", getOne(ds1, "SELECT COUNT(*) FROM unused"));
        this.fixtureController.finish();
        this.fixtureController.clearTables();
        assertEquals("1", getOne(ds1, "SELECT COUNT(*) FROM unused"));
    }

    @Test
    public void testClearTablesClearsUsedTables() throws SQLException {
        fixtureController.setDbContents("db1", getStream(PREFIX, "db1-minimal.yaml"));
        fixtureController.setDbContents("db2", getStream(PREFIX, "db2-minimal.yaml"));
        assertEquals("0", getOne(ds1, "SELECT COUNT(*) FROM user"));
        assertEquals("0", getOne(ds2, "SELECT COUNT(*) FROM address"));
        this.fixtureController.finish();
        assertEquals("1", getOne(ds1, "SELECT COUNT(*) FROM user"));
        assertEquals("1", getOne(ds2, "SELECT COUNT(*) FROM address"));
        this.fixtureController.clearTables();
        assertEquals("0", getOne(ds1, "SELECT COUNT(*) FROM user"));
        assertEquals("0", getOne(ds2, "SELECT COUNT(*) FROM address"));
    }

    @Test
    public void testBasicDeps() throws FileNotFoundException, SQLException {
        fixtureController.setDbContents("db1", getStream(PREFIX, "db1-basicDeps.yaml"));
        fixtureController.finish();

        // jane's otherId ref to joe's id
        int joeId = fixtureController.get("db1", "fx1", "user", "joe", "id").asInt();
        assertEquals("jane", fixtureController.get("db1", "fx1", "user", "jane", "name").asString());
        assertEquals("jane", getOne(ds1, "SELECT name FROM user WHERE otherId = " + joeId));

        // reading anon works
        assertEquals("anon", fixtureController.get("db1", "fxAnon", "user", "name").asString());

        // anonRef's name ref to anon's name
        assertEquals("anon", fixtureController.get("db1", "fx2", "user", "anonRef", "name").asString());
        assertEquals("anon", getOne(ds1, "SELECT name FROM user WHERE id = " +
                fixtureController.get("db1", "fx2", "user", "anonRef", "id").asInt()));

        // joeRef's id ref to joe from a different fixture
        assertEquals("joe", fixtureController.get("db1", "fx2", "user", "joeRef", "name").asString());
        assertEquals("joe", getOne(ds1, "SELECT name FROM user WHERE id = " +
                fixtureController.get("db1", "fx2", "user", "joeRef", "id").asInt()));
    }

    @Test
    public void testSpecialScalars() throws SQLException {
        fixtureController.setDbContents("db1", getStream(PREFIX, "db1-specialScalars.yaml"));
        fixtureController.finish();


        assertEquals("123", fixtureController.get("db1", "fx1", "user", "int", "name").asString());
        assertEquals("123", getOne(ds1, "SELECT name FROM user WHERE id = " +
                fixtureController.get("db1", "fx1", "user", "int", "id").asInt()));

        assertEquals("123.45", fixtureController.get("db1", "fx1", "user", "float", "name").asString());
        assertEquals("123.45", getOne(ds1, "SELECT name FROM user WHERE id = " +
                fixtureController.get("db1", "fx1", "user", "float", "id").asInt()));

        assertEquals("true", fixtureController.get("db1", "fx1", "user", "bool", "name").asString());
        assertEquals("true", getOne(ds1, "SELECT name FROM user WHERE id = " +
                fixtureController.get("db1", "fx1", "user", "bool", "id").asInt()));

        assertEquals("1234-12-12", fixtureController.get("db1", "fx1", "user", "timestamp", "name").asString());
        assertEquals("1234-12-12", getOne(ds1, "SELECT name FROM user WHERE id = " +
                fixtureController.get("db1", "fx1", "user", "timestamp", "id").asInt()));

        assertEquals("null", fixtureController.get("db1", "fx1", "user", "null", "name").asString());
        assertEquals("null", getOne(ds1, "SELECT name FROM user WHERE id = " +
                fixtureController.get("db1", "fx1", "user", "null", "id").asInt()));
    }

    @Test
    public void testCrossDbDeps() throws SQLException {
        fixtureController.setDbContents("db1", getStream(PREFIX, "db1-crossDb.yaml"));
        fixtureController.setDbContents("db2", getStream(PREFIX, "db2-crossDb.yaml"));
        fixtureController.finish();

        // address has standalone user id
        int standaloneUserId = fixtureController.get("db1", "fx1", "user", "standalone", "id").asInt();
        assertEquals(standaloneUserId, fixtureController.get("db2", "fx1", "address", "needsUser", "userId").asInt());
        assertEquals("" + standaloneUserId, getOne(ds2, "SELECT userId FROM address WHERE id = " +
                fixtureController.get("db2", "fx1", "address", "needsUser", "id").asInt()));

        // user has standalone address id

        int standaloneAddressId = fixtureController.get("db2", "fx1", "address", "standalone", "id").asInt();
        assertEquals(standaloneAddressId,
                fixtureController.get("db1", "fx1", "user", "needsAddress", "otherId").asInt());
        assertEquals("" + standaloneAddressId, getOne(ds1, "SELECT otherId FROM user WHERE id = " +
                fixtureController.get("db1", "fx1", "user", "needsAddress", "id").asInt()));
    }

    @Test
    public void testEmptyRowWithAutoId() {
        fixtureController.setDbContents("db1", getStream(PREFIX, "db1-emptyRow-withAutoId.yaml"));
        fixtureController.finish();
        assertTrue(fixtureController.get("db1", "fx1", "user", "withAutoId", "id").asInt() > 0);
    }

    @Test
    public void testNoAutoId() throws SQLException {
        fixtureController.setDbContents("db1", getStream(PREFIX, "db1-noAutoId.yaml"));
        fixtureController.finish();

        assertEquals(1, fixtureController.get("db1", "fx1", "user", "joe", "id").asInt());
    }

    protected static InputStream getStream(String prefix, String path) {
        InputStream stream = FixtureControllerTest.class.getResourceAsStream(prefix + "/" + path);
        if (stream == null) {
            throw new IllegalArgumentException("Couldn't find " + path);
        }
        return stream;
    }

    private static String getPathAsString(String path) throws IOException {
        InputStream sqlStream = getStream(PREFIX, path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(sqlStream));

        String latest = reader.readLine();
        StringBuilder buf = new StringBuilder();
        while (latest != null) {
            buf.append(latest);
            buf.append("\n");
            latest = reader.readLine();
        }

        reader.close();
        return buf.toString();
    }

    protected static String getOne(DataSource dataSource, String sql) throws SQLException {
        Connection connection = dataSource.getConnection();
        String result;
        try {
            Statement statement = connection.createStatement();
            try {
                ResultSet resultSet = statement.executeQuery(sql);
                try {
                    if (!resultSet.next()) {
                        throw new FixtureException("Nothing found for " + sql);
                    }
                    result = resultSet.getString(1);
                    if (resultSet.next()) {
                        throw new FixtureException("More than one row for " + sql);
                    }
                } finally {
                    resultSet.close();
                }
            } finally {
                statement.close();
            }
        } finally {
            connection.close();
        }

        return result;
    }

    protected static void execute(DataSource dataSource, String sql) throws SQLException {
        Connection connection = dataSource.getConnection();
        try {
            Statement statement = connection.createStatement();
            try {
                statement.execute(sql);
            } finally {
                statement.close();
            }
        } finally {
            connection.close();
        }
    }
}
