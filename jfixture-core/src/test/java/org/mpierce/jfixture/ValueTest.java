/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ValueTest {

    @Test
    public void testAsInt() {
        assertEquals(123, getValue("123").asInt());
    }

    @Test
    public void testAsLong() {
        assertEquals(123456789012345L, getValue("123456789012345").asLong());
    }

    @Test
    public void testAsFloat() {
        assertEquals(123.45, getValue("123.45").asFloat(), .0001);
    }

    @Test
    public void testAsDouble() {
        assertEquals(1230000000000000000000.45D, getValue("1230000000000000000000.45").asDouble(), .00001);
    }

    @Test
    public void testAsBoolean() {
        assertEquals(true, getValue("true").asBoolean());
        assertEquals(true, getValue("TRUE").asBoolean());
        assertEquals(true, getValue("TRue").asBoolean());
        assertEquals(false, getValue("false").asBoolean());
        assertEquals(false, getValue("False").asBoolean());
        assertEquals(false, getValue("FALSE").asBoolean());
    }

    @Test
    public void testAsString() {
        assertEquals("asdf", getValue("asdf").asString());
    }

    @Test
    public void testToString() {
        assertEquals("Value[fixtureGroupName/fixtureName.tableName.(only row).colName=foo]",
                getValue("foo").toString());
    }

    private static Value getValue(String contents) {
        FixtureGroupRepository repos = new FixtureGroupRepository();
        FixtureGroup fixtureGroup = new FixtureGroup(repos, new DbIdentifier("fixtureGroupName"));
        repos.add(fixtureGroup);
        Fixture fixture = new Fixture(fixtureGroup, new FixtureIdentifier("fixtureName"));
        fixtureGroup.add(fixture);
        RowBundle<Row> rowBundle = new AnonymousRowBundle(fixture, new TableIdentifier("tableName"));
        fixture.add(rowBundle);
        Row row = new Row(rowBundle, new AnonymousRowIdentifier());
        rowBundle.setContents(row);
        Value value = new Value(row, new ColumnIdentifier("colName"), contents);
        row.add(value);
        return value;
    }
}
