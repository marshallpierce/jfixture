/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class RowDependencyNodeTest {

    private RowDependencyNode node1 = null;

    private RowDependencyNode node2 = null;

    private RowDependencyNode node3 = null;

    @Before
    public void setUp() {
        this.node1 = new RowDependencyNode(null);
        this.node2 = new RowDependencyNode(null);
        this.node3 = new RowDependencyNode(null);
    }

    @Test
    public void testAddIncomingWithWrongEnd() {
        RowDependencyEdge edge = new RowDependencyEdge(this.node1, this.node2);

        try {
            this.node3.addIncoming(edge);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("An edge was added as incoming that did not end at this node", e.getMessage());
        }
    }

    @Test
    public void testAddOutgoingWithWrongStart() {
        RowDependencyEdge edge = new RowDependencyEdge(this.node1, this.node2);

        try {
            this.node3.addOutgoing(edge);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("An edge was added as outgoing that did not start at this node", e.getMessage());
        }
    }

    @Test
    public void testAddDuplicateIncoming() {
        RowDependencyEdge edge = new RowDependencyEdge(this.node1, this.node2);

        this.node2.addIncoming(edge);

        try {
            this.node2.addIncoming(edge);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("An incoming edge was added twice", e.getMessage());
        }
    }

    @Test
    public void testAddDuplicateOutgoing() {
        RowDependencyEdge edge = new RowDependencyEdge(this.node1, this.node2);

        this.node1.addOutgoing(edge);

        try {
            this.node1.addOutgoing(edge);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("An outgoing edge was added twice", e.getMessage());
        }
    }

    @Test
    public void testGetDependencies() {
        this.node1.addOutgoing(new RowDependencyEdge(this.node1, this.node2));
        this.node1.addOutgoing(new RowDependencyEdge(this.node1, this.node3));

        Set<RowDependencyNode> nodeSet = this.node1.getDependencies();
        assertEquals(2, nodeSet.size());
        assertTrue(nodeSet.contains(this.node2));
        assertTrue(nodeSet.contains(this.node3));
    }
}
