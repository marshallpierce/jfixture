/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class RowDependencyGraphTest {
    private RowDependencyGraph graph = null;
    private static final int NUM_RUNS = 100;

    @Before
    public void setUp() {
        this.graph = new RowDependencyGraph();
    }

    @Test
    public void testEntireGraphIsACycleFails() {
        Row r1 = getRow(1);
        Row r2 = getRow(2);

        this.graph.addDependency(r1, r2);
        try {
            this.graph.addDependency(r2, r1);
            fail();
        } catch (FixtureException e) {
            assertEquals("Cyclic dependency detected: fxGroup/fx.tbl[1] -> fxGroup/fx.tbl[2] depends on " +
                    "fxGroup/fx.tbl[1] which already is in the path", e.getMessage());
        }
    }

    @Test
    public void testSubGraphIsACycleFails() {
        Row r1 = getRow(1);
        Row r2 = getRow(2);
        Row r3 = getRow(3);

        this.graph.addDependency(r1, r2);
        this.graph.addDependency(r2, r3);

        try {
            this.graph.addDependency(r3, r2);
            fail();
        } catch (FixtureException e) {
            assertEquals(
                    "Cyclic dependency detected: fxGroup/fx.tbl[1] -> fxGroup/fx.tbl[2] -> fxGroup/fx.tbl[3] depends " +
                            "on fxGroup/fx.tbl[2] which already is in the path", e.getMessage());
        }
    }

    @Test
    public void testAddSameDependency() {
        Row r1 = getRow(1);
        Row r2 = getRow(2);

        this.graph.addDependency(r1, r2);
        this.graph.addDependency(r1, r2);

        assertEquals(Arrays.asList(r2, r1), this.graph.getRowsInInsertionOrder());
    }

    @Test
    public void testGetRowsInInsertionOrder() {
        Row r1 = getRow(1);
        Row r2 = getRow(2);
        Row r3 = getRow(3);
        Row r4 = getRow(4);

        this.graph.addDependency(r1, r2);
        this.graph.addDependency(r2, r3);
        this.graph.addDependency(r3, r4);

        List<Row> rowList = this.graph.getRowsInInsertionOrder();

        List<Row> expected = Arrays.asList(r4, r3, r2, r1);
        assertEquals(expected, rowList);
    }

    @Test
    public void testGetRowsInInsertionOrderAlwaysGetsSameListContents() {
        Row r1 = getRow(1);
        Row r2 = getRow(2);
        Row r3 = getRow(3);
        Row r4 = getRow(4);
        Row r5 = getRow(5);

        this.graph.addDependency(r1, r2);
        this.graph.addDependency(r1, r3);
        this.graph.addDependency(r1, r5);
        this.graph.addDependency(r2, r3);
        this.graph.addDependency(r2, r4);
        this.graph.addDependency(r3, r4);

        List<Row> rows = this.graph.getRowsInInsertionOrder();

        for (int i = 0; i < NUM_RUNS; i++) {
            assertEquals(rows, this.graph.getRowsInInsertionOrder());
        }
    }

    @Test
    public void testGetRowsInInsertionOrderFindsLongestPath() {
        Row r1 = getRow(1);
        Row r2 = getRow(2);
        Row r3 = getRow(3);
        Row r4 = getRow(4);
        Row r5 = getRow(5);

        GraphInsertionOrderTester tester = new GraphInsertionOrderTester();

        tester.addDependency(r1, r2);
        tester.addDependency(r1, r3);
        tester.addDependency(r1, r5);
        tester.addDependency(r2, r3);
        tester.addDependency(r2, r4);
        tester.addDependency(r3, r4);

        tester.runTests(NUM_RUNS);
    }

    @Test
    public void testGetRowsInInsertionOrderFindsLongestPath2() {
        Row r1 = getRow(1);
        Row r2 = getRow(2);
        Row r3 = getRow(3);
        Row r4 = getRow(4);
        Row r5 = getRow(5);

        GraphInsertionOrderTester tester = new GraphInsertionOrderTester();

        tester.addDependency(r1, r2);
        tester.addDependency(r1, r3);
        tester.addDependency(r1, r4);
        tester.addDependency(r2, r5);
        tester.addDependency(r3, r5);
        tester.addDependency(r4, r5);

        tester.runTests(NUM_RUNS);
    }

    @Test
    public void testGetRowsInInsertionOrderWithForest() {
        Row r1 = getRow(1);
        Row r2 = getRow(2);
        Row r3 = getRow(3);
        Row r4 = getRow(4);

        Row s1 = getRow(1);
        Row s2 = getRow(2);
        Row s3 = getRow(3);
        Row s4 = getRow(4);

        GraphInsertionOrderTester tester = new GraphInsertionOrderTester();

        tester.addDependency(r1, r2);
        tester.addDependency(r1, r3);
        tester.addDependency(r2, r4);
        tester.addDependency(r3, r4);

        tester.addDependency(s1, s2);
        tester.addDependency(s1, s3);
        tester.addDependency(s2, s4);
        tester.addDependency(s3, s4);

        tester.runTests(NUM_RUNS);
    }

    private static Row getRow(int rowPosition) {
        return RowTest.getRow(new IndexedRowIdentifier(rowPosition));
    }

    /**
     * Helper to test that dependencies are all respected in the insertion order. Many runs are used to make sure there
     * aren't any subtle dependencies on hash key order, etc that aren't already LinkedHashSet'd.
     */
    private class GraphInsertionOrderTester {

        final List<Row[]> dependencies = new ArrayList<Row[]>();

        private void addDependency(Row start, Row end) {
            dependencies.add(new Row[]{start, end});
        }

        private void runTests(int numRuns) {
            for (int i = 0; i < numRuns; i++) {
                setUp();

                for (Row[] dependency : this.dependencies) {
                    Row start = dependency[0];
                    Row end = dependency[1];

                    graph.addDependency(start, end);
                }

                this.checkDependencies();
            }
        }

        @SuppressWarnings({"NonBooleanMethodNameMayNotStartWithQuestion"})
        private void checkDependencies() {

            List<Row> rows = graph.getRowsInInsertionOrder();

            for (Row[] dependency : this.dependencies) {
                Row start = dependency[0];
                Row end = dependency[1];

                assertBefore(end, start, rows);
            }
        }

        private void assertBefore(Row before, Row after, List<Row> rows) {
            int beforeIndex = rows.indexOf(before);
            int afterIndex = rows.indexOf(after);

            assertTrue(beforeIndex != -1);
            assertTrue(afterIndex != -1);

            assertTrue("Expecting " + before + " before " + after + " in " + rows, beforeIndex < afterIndex);
        }
    }
}
