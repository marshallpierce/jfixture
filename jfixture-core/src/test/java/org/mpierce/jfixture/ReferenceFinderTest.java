/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@SuppressWarnings({"ConstantConditions"})
public class ReferenceFinderTest {

    private ReferenceFinder finder = null;

    @Before
    public void setUp() {
        this.finder = new ReferenceFinder();
    }

    @Test
    public void testDetectsMultipleAutoIdColumns() {
        FixtureGroupRepository repos = new FixtureGroupRepository();
        FixtureGroup fixtureGroup = new FixtureGroup(repos, new DbIdentifier("fixtureGroupName"));
        repos.add(fixtureGroup);
        Fixture fixture = new Fixture(fixtureGroup, new FixtureIdentifier("fixtureName"));
        fixtureGroup.add(fixture);
        RowBundle<Row> rowBundle = new AnonymousRowBundle(fixture, new TableIdentifier("tableName"));
        fixture.add(rowBundle);
        Row row = new Row(rowBundle, new AnonymousRowIdentifier());
        rowBundle.setContents(row);
        row.add(new Value(row, new ColumnIdentifier("colName"), "${autoId}"));
        row.add(new Value(row, new ColumnIdentifier("colName2"), "${autoId}"));
        try {
            this.finder.traverse(repos);
            fail();
        } catch (FixtureException e) {
            assertEquals("context: [fixtureGroupName=fixtureGroupName, fixtureName=fixtureName, tableName=tableName, " +
                    "row=(only row), column=colName2, value=${autoId}] Two auto-id values in the same row detected",
                    e.getMessage());
        }
    }

    @Test
    public void testResolveThis() {
        ColumnReference ref = this.getRef("${this.tblName.colName}");
        assertEquals("fixtureGroupName", ref.getDbIdentifier().toString());
        assertEquals("fixtureName", ref.getFixtureIdentifier().toString());
        assertEquals("tblName", ref.getTableName().toString());
        assertEquals(RowIdentifierType.ANONYMOUS_ROW, ref.getRowIdentifierType());
        assertTrue(this.finder.getAutoIdValues().isEmpty());
        assertFalse(this.finder.getValuesWithRefs().isEmpty());
        assertEquals(ref.toString(),
                this.finder.getValuesWithRefs().entrySet().iterator().next().getValue().toString());
    }

    @Test
    public void testFindNoRef() {
        assertNull(this.getRef("nonRef"));
        assertTrue(this.finder.getAutoIdValues().isEmpty());
        assertTrue(this.finder.getValuesWithRefs().isEmpty());
    }

    @Test
    public void testAutoIsNotARef() {
        assertNull(this.getRef("${autoId}"));
        assertFalse(this.finder.getAutoIdValues().isEmpty());
        assertEquals("${autoId}", this.finder.getAutoIdValues().entrySet().iterator().next().getValue().asString());
        assertTrue(this.finder.getValuesWithRefs().isEmpty());
    }

    @Test
    public void testFindUnscopedAnonymousColRef() {
        ColumnReference ref = this.getRef("${fixName.tblName.colName}");
        assertEquals("fixtureGroupName", ref.getDbIdentifier().toString());
        assertEquals("fixName", ref.getFixtureIdentifier().toString());
        assertEquals("tblName", ref.getTableName().toString());
        assertEquals(RowIdentifierType.ANONYMOUS_ROW, ref.getRowIdentifierType());
        assertTrue(this.finder.getAutoIdValues().isEmpty());
        assertFalse(this.finder.getValuesWithRefs().isEmpty());
        assertEquals(ref.toString(),
                this.finder.getValuesWithRefs().entrySet().iterator().next().getValue().toString());
    }

    @Test
    public void testFindUnscopedIndexedColRef() {
        ColumnReference ref = this.getRef("${fixName.tblName[0].colName}");
        assertEquals("fixtureGroupName", ref.getDbIdentifier().toString());
        assertEquals("fixName", ref.getFixtureIdentifier().toString());
        assertEquals("tblName", ref.getTableName().toString());
        assertEquals(RowIdentifierType.INDEXED_ROW, ref.getRowIdentifierType());
        assertEquals(0, ref.getRowIdentifier().getRowPosition());
        assertTrue(this.finder.getAutoIdValues().isEmpty());
        assertFalse(this.finder.getValuesWithRefs().isEmpty());
        assertEquals(ref.toString(),
                this.finder.getValuesWithRefs().entrySet().iterator().next().getValue().toString());
    }

    @Test
    public void testFindUnscopedNamedColRef() {
        ColumnReference ref = this.getRef("${fixName.tblName{rowName}.colName}");
        assertEquals("fixtureGroupName", ref.getDbIdentifier().toString());
        assertEquals("fixName", ref.getFixtureIdentifier().toString());
        assertEquals("tblName", ref.getTableName().toString());
        assertEquals(RowIdentifierType.NAMED_ROW, ref.getRowIdentifierType());
        assertEquals("rowName", ref.getRowIdentifier().getRowName());
        assertTrue(this.finder.getAutoIdValues().isEmpty());
        assertFalse(this.finder.getValuesWithRefs().isEmpty());
        assertEquals(ref.toString(),
                this.finder.getValuesWithRefs().entrySet().iterator().next().getValue().toString());
    }

    @Test
    public void testFindScopedAnonymousColRef() {
        ColumnReference ref = this.getRef("${db2/fixName.tblName.colName}");
        assertEquals("db2", ref.getDbIdentifier().toString());
        assertEquals("fixName", ref.getFixtureIdentifier().toString());
        assertEquals("tblName", ref.getTableName().toString());
        assertEquals(RowIdentifierType.ANONYMOUS_ROW, ref.getRowIdentifierType());
        assertTrue(this.finder.getAutoIdValues().isEmpty());
        assertFalse(this.finder.getValuesWithRefs().isEmpty());
        assertEquals(ref.toString(),
                this.finder.getValuesWithRefs().entrySet().iterator().next().getValue().toString());
    }

    @Test
    public void testFindScopedIndexedColRef() {
        ColumnReference ref = this.getRef("${db2/fixName.tblName[0].colName}");
        assertEquals("db2", ref.getDbIdentifier().toString());
        assertEquals("fixName", ref.getFixtureIdentifier().toString());
        assertEquals("tblName", ref.getTableName().toString());
        assertEquals(RowIdentifierType.INDEXED_ROW, ref.getRowIdentifierType());
        assertEquals(0, ref.getRowIdentifier().getRowPosition());
        assertTrue(this.finder.getAutoIdValues().isEmpty());
        assertFalse(this.finder.getValuesWithRefs().isEmpty());
        assertEquals(ref.toString(),
                this.finder.getValuesWithRefs().entrySet().iterator().next().getValue().toString());
    }

    @Test
    public void testFindScopedNamedColRef() {
        ColumnReference ref = this.getRef("${db2/fixName.tblName{rowName}.colName}");
        assertEquals("db2", ref.getDbIdentifier().toString());
        assertEquals("fixName", ref.getFixtureIdentifier().toString());
        assertEquals("tblName", ref.getTableName().toString());
        assertEquals(RowIdentifierType.NAMED_ROW, ref.getRowIdentifierType());
        assertEquals("rowName", ref.getRowIdentifier().getRowName());
        assertTrue(this.finder.getAutoIdValues().isEmpty());
        assertFalse(this.finder.getValuesWithRefs().isEmpty());
        assertEquals(ref.toString(),
                this.finder.getValuesWithRefs().entrySet().iterator().next().getValue().toString());
    }

    /**
     * @param valueContents the contents of the value
     *
     * @return the ref in the valueContents
     *
     * @throws FixtureException
     */
    @Nullable
    private ColumnReference getRef(String valueContents) {
        FixtureGroupRepository repos = new FixtureGroupRepository();
        FixtureGroup fixtureGroup = new FixtureGroup(repos, new DbIdentifier("fixtureGroupName"));
        repos.add(fixtureGroup);
        Fixture fixture = new Fixture(fixtureGroup, new FixtureIdentifier("fixtureName"));
        fixtureGroup.add(fixture);
        RowBundle<Row> rowBundle = new AnonymousRowBundle(fixture, new TableIdentifier("tableName"));
        fixture.add(rowBundle);
        Row row = new Row(rowBundle, new AnonymousRowIdentifier());
        rowBundle.setContents(row);
        row.add(new Value(row, new ColumnIdentifier("colName"), valueContents));
        this.finder.traverse(repos);
        Set<RowLink> refs = this.finder.getRefs();
        if (refs.size() == 0) {
            return null;
        }
        if (refs.size() > 1) {
            throw new IllegalStateException("Got " + refs.size() + " results");
        }
        return refs.iterator().next().getColumnReference();
    }
}
