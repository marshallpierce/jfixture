/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FixtureControllerTest {
    private FixtureController fixtureController;
    private static final String PREFIX = "FixtureControllerTest";

    @Before
    public void setUp() {
        this.fixtureController = new FixtureController();
        this.fixtureController.addDb("db1", EasyMock.createStrictMock(DataSource.class));
    }

    @Test
    public void testCantAddDbTwice() {
        try {
            this.fixtureController.addDb("db1", EasyMock.createStrictMock(DataSource.class));
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("A value already exists for key db1", e.getMessage());
        }
    }

    @Test
    public void testCantSetDbContentsTwice() {
        this.fixtureController
                .setDbContents("db1", ConnectionProviderTestCase.getStream(PREFIX, "db1-circularDeps.yaml"));
        try {
            this.fixtureController
                    .setDbContents("db1", ConnectionProviderTestCase.getStream(PREFIX, "db1-circularDeps.yaml"));
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("A value already exists for key db1", e.getMessage());
        }
    }

    @Test
    public void testCircularDeps() throws SQLException {
        fixtureController.setDbContents("db1", ConnectionProviderTestCase.getStream(PREFIX, "db1-circularDeps.yaml"));
        try {
            fixtureController.finish();
            fail();
        } catch (FixtureException e) {
            // order in which the dependency is detected is basically random
            String msg = e.getMessage();
            if (msg.contains("{jane} ->")) {
                assertEquals(
                        "Cyclic dependency detected: db1/fx1.user{jane} -> db1/fx1.user{joe} depends on db1/fx1.user{jane} " +
                                "which already is in the path", msg);
            } else {
                assertEquals("Cyclic dependency detected: db1/fx1.user{joe} -> db1/fx1.user{jane} depends on " +
                        "db1/fx1.user{joe} which already is in the path", msg);
            }
        }
    }
}
