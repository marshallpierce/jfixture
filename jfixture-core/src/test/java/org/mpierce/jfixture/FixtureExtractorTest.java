/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class FixtureExtractorTest {

    private FixtureExtractor extractor = null;

    @Before
    public void setUp() {
        this.extractor = new FixtureExtractor();
    }

    @Test
    public void testExtractAnonymousRow() {
        FixtureGroup group = extract("anonymousRow.yaml");
        assertEquals("dbName", group.getIdentifier().toString());

        Fixture fixt1 = group.get(new FixtureIdentifier("fixt1"));
        Fixture fixt2 = group.get(new FixtureIdentifier("fixt2"));

        assertEquals(2, group.size());

        assertEquals("fixt1", fixt1.getIdentifier().toString());
        assertEquals("fixt2", fixt2.getIdentifier().toString());

        assertSame(group, fixt1.getContainer());
        assertSame(group, fixt2.getContainer());

        assertEquals(2, fixt1.size());
        assertEquals(2, fixt2.size());

        RowBundle<?> tbl1 = fixt1.get(new TableIdentifier("tbl1"));
        RowBundle<?> tbl2 = fixt1.get(new TableIdentifier("tbl2"));

        RowBundle<?> tbl3 = fixt2.get(new TableIdentifier("tbl3"));
        RowBundle<?> tbl4 = fixt2.get(new TableIdentifier("tbl4"));

        assertEquals("tbl1", tbl1.getIdentifier().toString());
        assertEquals("tbl2", tbl2.getIdentifier().toString());
        assertEquals("tbl3", tbl3.getIdentifier().toString());
        assertEquals("tbl4", tbl4.getIdentifier().toString());

        assertSame(fixt1, tbl1.getContainer());
        assertSame(fixt1, tbl2.getContainer());
        assertSame(fixt2, tbl3.getContainer());
        assertSame(fixt2, tbl4.getContainer());

        assertEquals(RowIdentifierType.ANONYMOUS_ROW, tbl1.getType());
        assertEquals(RowIdentifierType.ANONYMOUS_ROW, tbl2.getType());
        assertEquals(RowIdentifierType.ANONYMOUS_ROW, tbl3.getType());
        assertEquals(RowIdentifierType.ANONYMOUS_ROW, tbl4.getType());

        assertEquals(1, tbl1.size());
        assertEquals(1, tbl2.size());
        assertEquals(1, tbl3.size());
        assertEquals(1, tbl4.size());

        Row row1 = tbl1.get(new AnonymousRowIdentifier());
        Row row2 = tbl2.get(new AnonymousRowIdentifier());
        Row row3 = tbl3.get(new AnonymousRowIdentifier());
        Row row4 = tbl4.get(new AnonymousRowIdentifier());

        assertEquals(2, row1.size());
        assertEquals(2, row2.size());
        assertEquals(2, row3.size());
        assertEquals(2, row4.size());

        assertEquals(tbl1, row1.getContainer());
        assertEquals(tbl2, row2.getContainer());
        assertEquals(tbl3, row3.getContainer());
        assertEquals(tbl4, row4.getContainer());

        assertEquals("(only row)", row1.getIdentifier().toString());
        assertEquals("(only row)", row2.getIdentifier().toString());
        assertEquals("(only row)", row3.getIdentifier().toString());
        assertEquals("(only row)", row4.getIdentifier().toString());

        Value val1 = row1.get(new ColumnIdentifier("col1"));
        Value val2 = row1.get(new ColumnIdentifier("col2"));
        Value val3 = row2.get(new ColumnIdentifier("col3"));
        Value val4 = row2.get(new ColumnIdentifier("col4"));
        Value val5 = row3.get(new ColumnIdentifier("col5"));
        Value val6 = row3.get(new ColumnIdentifier("col6"));
        Value val7 = row4.get(new ColumnIdentifier("col7"));
        Value val8 = row4.get(new ColumnIdentifier("col8"));

        assertEquals("val1", val1.asString());
        assertEquals("val2", val2.asString());
        assertEquals("val3", val3.asString());
        assertEquals("val4", val4.asString());
        assertEquals("val5", val5.asString());
        assertEquals("val6", val6.asString());
        assertEquals("val7", val7.asString());
        assertEquals("val8", val8.asString());

        assertEquals("col1", val1.getIdentifier().toString());
        assertEquals("col2", val2.getIdentifier().toString());
        assertEquals("col3", val3.getIdentifier().toString());
        assertEquals("col4", val4.getIdentifier().toString());
        assertEquals("col5", val5.getIdentifier().toString());
        assertEquals("col6", val6.getIdentifier().toString());
        assertEquals("col7", val7.getIdentifier().toString());
        assertEquals("col8", val8.getIdentifier().toString());

        assertSame(row1, val1.getContainer());
        assertSame(row1, val2.getContainer());
        assertSame(row2, val3.getContainer());
        assertSame(row2, val4.getContainer());
        assertSame(row3, val5.getContainer());
        assertSame(row3, val6.getContainer());
        assertSame(row4, val7.getContainer());
        assertSame(row4, val8.getContainer());
    }

    @Test
    public void testExtractRowList() {
        FixtureGroup group = extract("rowList.yaml");
        assertEquals("dbName", group.getIdentifier().toString());

        assertEquals(1, group.size());

        Fixture fixt1 = group.get(new FixtureIdentifier("fixt1"));

        assertEquals("fixt1", fixt1.getIdentifier().toString());

        assertSame(group, fixt1.getContainer());

        assertEquals(1, fixt1.size());

        RowBundle<?> tbl1 = fixt1.get(new TableIdentifier("tbl1"));

        assertEquals(RowIdentifierType.INDEXED_ROW, tbl1.getType());

        assertEquals(2, tbl1.size());

        assertEquals("tbl1", tbl1.getIdentifier().toString());

        assertSame(fixt1, tbl1.getContainer());

        Row row1 = tbl1.get(new IndexedRowIdentifier(0));
        Row row2 = tbl1.get(new IndexedRowIdentifier(1));

        assertEquals(2, row1.size());
        assertEquals(2, row2.size());

        assertEquals(tbl1, row1.getContainer());
        assertEquals(tbl1, row2.getContainer());

        assertEquals("0", row1.getIdentifier().toString());
        assertEquals("1", row2.getIdentifier().toString());

        Value val1 = row1.get(new ColumnIdentifier("col1"));
        Value val2 = row1.get(new ColumnIdentifier("col2"));
        Value val3 = row2.get(new ColumnIdentifier("col3"));
        Value val4 = row2.get(new ColumnIdentifier("col4"));

        assertEquals("val1", val1.asString());
        assertEquals("val2", val2.asString());
        assertEquals("val3", val3.asString());
        assertEquals("val4", val4.asString());

        assertEquals("col1", val1.getIdentifier().toString());
        assertEquals("col2", val2.getIdentifier().toString());
        assertEquals("col3", val3.getIdentifier().toString());
        assertEquals("col4", val4.getIdentifier().toString());

        assertSame(row1, val1.getContainer());
        assertSame(row1, val2.getContainer());
        assertSame(row2, val3.getContainer());
        assertSame(row2, val4.getContainer());
    }

    @Test
    public void testExtractNamedRows() {
        FixtureGroup group = extract("namedRows.yaml");
        assertEquals("dbName", group.getIdentifier().toString());

        Fixture fixt1 = group.get(new FixtureIdentifier("fixt1"));

        assertEquals(1, group.size());

        assertEquals("fixt1", fixt1.getIdentifier().toString());

        assertSame(group, fixt1.getContainer());

        assertEquals(1, fixt1.size());

        RowBundle<?> tbl1 = fixt1.get(new TableIdentifier("tbl1"));

        assertEquals(RowIdentifierType.NAMED_ROW, tbl1.getType());

        assertEquals("tbl1", tbl1.getIdentifier().toString());

        assertSame(fixt1, tbl1.getContainer());

        assertEquals(2, tbl1.size());

        Row row1 = tbl1.get(new NamedRowIdentifier("row1"));
        Row row2 = tbl1.get(new NamedRowIdentifier("row2"));

        assertEquals(2, row1.size());
        assertEquals(2, row2.size());

        assertEquals(tbl1, row1.getContainer());
        assertEquals(tbl1, row2.getContainer());

        assertEquals("row1", row1.getIdentifier().toString());
        assertEquals("row2", row2.getIdentifier().toString());

        Value val1 = row1.get(new ColumnIdentifier("col1"));
        Value val2 = row1.get(new ColumnIdentifier("col2"));
        Value val3 = row2.get(new ColumnIdentifier("col3"));
        Value val4 = row2.get(new ColumnIdentifier("col4"));

        assertEquals("val1", val1.asString());
        assertEquals("val2", val2.asString());
        assertEquals("val3", val3.asString());
        assertEquals("val4", val4.asString());

        assertEquals("col1", val1.getIdentifier().toString());
        assertEquals("col2", val2.getIdentifier().toString());
        assertEquals("col3", val3.getIdentifier().toString());
        assertEquals("col4", val4.getIdentifier().toString());

        assertSame(row1, val1.getContainer());
        assertSame(row1, val2.getContainer());
        assertSame(row2, val3.getContainer());
        assertSame(row2, val4.getContainer());
    }

    /**
     * @param path resource path inside fixture dir
     *
     * @return the fixture group from the yaml file
     *
     * @throws FixtureException
     */
    private FixtureGroup extract(String path) {
        String fullPath = "FixtureExtractorTest/" + path;
        InputStream stream = this.getClass().getResourceAsStream(fullPath);

        if (stream == null) {
            throw new IllegalArgumentException("Couldn't find anything for " + fullPath);
        }

        Object parsedYaml = YamlData.getYamlData(stream);
        return this.extractor
                .extractUnresolvedFixtures(new FixtureGroupRepository(), new DbIdentifier("dbName"), parsedYaml);
    }
}
