/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

public class ColumnReferenceResolverTest {

    private ColumnReferenceResolver resolver;

    private Value namedRowValue;
    private Value indexedRowValue;
    private Value anonRowValue;

    @Before
    public void setUp() {
        FixtureGroupRepository repos = new FixtureGroupRepository();
        FixtureGroup fixtureGroup = new FixtureGroup(repos, new DbIdentifier("dbName"));
        repos.add(fixtureGroup);
        Fixture fixture = new Fixture(fixtureGroup, new FixtureIdentifier("fixtureName"));
        fixtureGroup.add(fixture);

        // table with an anonymous row
        AnonymousRowBundle anonBundle = new AnonymousRowBundle(fixture, new TableIdentifier("anonTable"));
        fixture.add(anonBundle);
        Row anonRow = new Row(anonBundle, new AnonymousRowIdentifier());
        anonBundle.setContents(anonRow);
        this.anonRowValue = new Value(anonRow, new ColumnIdentifier("colName"), "colValue");
        anonRow.add(this.anonRowValue);

        // table with a named row
        RowMapBundle namedBundle = new RowMapBundle(fixture, new TableIdentifier("namedRowTable"));
        fixture.add(namedBundle);
        String rowName = "namedRow1";
        Row namedRow = new Row(namedBundle, new NamedRowIdentifier(rowName));
        HashMap<String, Row> rowMap = new HashMap<String, Row>();
        rowMap.put(rowName, namedRow);
        namedBundle.setContents(rowMap);
        this.namedRowValue = new Value(namedRow, new ColumnIdentifier("colName"), "colValue");
        namedRow.add(this.namedRowValue);

        // table with an indexed row
        RowListBundle indexedBundle = new RowListBundle(fixture, new TableIdentifier("indexedRowTable"));
        fixture.add(indexedBundle);
        Row indexedRow = new Row(indexedBundle, new IndexedRowIdentifier(0));
        indexedBundle.setContents(Arrays.asList(indexedRow));
        this.indexedRowValue = new Value(indexedRow, new ColumnIdentifier("colName"), "colValue");
        indexedRow.add(this.indexedRowValue);

        this.resolver = new ColumnReferenceResolver(repos);
    }

    @Test
    public void testFindBadDbName() {
        ColumnReference ref = ColumnReference
                .getAnonymousRowRef(new DbIdentifier("badBName"), new FixtureIdentifier("fixture"),
                        new TableIdentifier("table"), new ColumnIdentifier("column"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals(
                    "context: [ref badBName/fixture.table.column, dbName badBName] Couldn't find the group for the db name",
                    e.getMessage());
        }
    }

    @Test
    public void testFindBadFixtureName() {
        ColumnReference ref = ColumnReference
                .getAnonymousRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("badFixture"),
                        new TableIdentifier("table"), new ColumnIdentifier("column"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals(
                    "context: [ref dbName/badFixture.table.column, dbName dbName, fixture name badFixture] Couldn't find the fixture",
                    e.getMessage());
        }
    }

    @Test
    public void testFindBadTableName() {
        ColumnReference ref = ColumnReference
                .getAnonymousRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("badTable"), new ColumnIdentifier("column"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals("context: [ref dbName/fixtureName.badTable.column, dbName dbName, fixture name fixtureName," +
                    " table name badTable] Couldn't find the rowBundle", e.getMessage());
        }
    }

    @Test
    public void testFindBadColumnName() {
        ColumnReference ref = ColumnReference
                .getAnonymousRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("anonTable"), new ColumnIdentifier("badColumn"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals(
                    "context: [ref dbName/fixtureName.anonTable.badColumn, dbName dbName, fixture name fixtureName," +
                            " table name anonTable, row id (only row), column name badColumn]" +
                            " Couldn't find column", e.getMessage());
        }
    }

    @Test
    public void testFindAnonRowWithNamedRef() {
        ColumnReference ref = ColumnReference
                .getNamedRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("anonTable"), "rowName", new ColumnIdentifier("badColumn"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals("context: [ref dbName/fixtureName.anonTable{rowName}.badColumn, dbName dbName, " +
                    "fixture name fixtureName, table name anonTable]" +
                    " row ref was of type NAMED_ROW but rowBundle was of incompatible type ANONYMOUS_ROW",
                    e.getMessage());
        }
    }

    @Test
    public void testFindIndexedRowWithRowName() {
        ColumnReference ref = ColumnReference
                .getNamedRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("indexedRowTable"), "rowName", new ColumnIdentifier("badColumn"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals("context: [ref dbName/fixtureName.indexedRowTable{rowName}.badColumn, dbName dbName," +
                    " fixture name fixtureName, table name indexedRowTable] " +
                    "row ref was of type NAMED_ROW but rowBundle was of incompatible type INDEXED_ROW", e.getMessage());
        }
    }

    @Test
    public void testFindIndexedRowWithWrongRowIndex() {
        ColumnReference ref = ColumnReference
                .getIndexedRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("indexedRowTable"), 2, new ColumnIdentifier("badColumn"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals("context: [ref dbName/fixtureName.indexedRowTable[2].badColumn, dbName dbName," +
                    " fixture name fixtureName, table name indexedRowTable, row id 2] " + "Couldn't find row",
                    e.getMessage());
        }
    }

    @Test
    public void testFindNamedRowWithWrongRowName() {
        ColumnReference ref = ColumnReference
                .getNamedRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("namedRowTable"), "badRowName", new ColumnIdentifier("badColumn"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals("context: [ref dbName/fixtureName.namedRowTable{badRowName}.badColumn, dbName dbName, " +
                    "fixture name fixtureName, table name namedRowTable, row id badRowName] " + "Couldn't find row",
                    e.getMessage());
        }
    }

    @Test
    public void testFindNamedRowWithAnonRef() {
        ColumnReference ref = ColumnReference
                .getAnonymousRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("namedRowTable"), new ColumnIdentifier("asdf"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals("context: [ref dbName/fixtureName.namedRowTable.asdf, dbName dbName, " +
                    "fixture name fixtureName, table name namedRowTable]" +
                    " row ref was of type ANONYMOUS_ROW but rowBundle was of incompatible type NAMED_ROW",
                    e.getMessage());
        }
    }

    @Test
    public void testFindIndexedRowWithAnonRef() {
        ColumnReference ref = ColumnReference
                .getAnonymousRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("indexedRowTable"), new ColumnIdentifier("asdf"));
        try {
            this.resolver.resolve(ref);
            fail();
        } catch (FixtureException e) {
            assertEquals(
                    "context: [ref dbName/fixtureName.indexedRowTable.asdf, dbName dbName, fixture name fixtureName, " +
                            "table name indexedRowTable]" +
                            " row ref was of type ANONYMOUS_ROW but rowBundle was of incompatible type INDEXED_ROW",
                    e.getMessage());
        }
    }

    @Test
    public void testFindNamedRowWithRightRowName() {
        ColumnReference ref = ColumnReference
                .getNamedRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("namedRowTable"), "namedRow1", new ColumnIdentifier("colName"));
        assertSame(this.namedRowValue, this.resolver.resolve(ref));
    }

    @Test
    public void testFindIndexedRowWithRightRowName() {
        ColumnReference ref = ColumnReference
                .getIndexedRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("indexedRowTable"), 0, new ColumnIdentifier("colName"));
        assertSame(this.indexedRowValue, this.resolver.resolve(ref));
    }

    @Test
    public void testFindAnonRowWithRightRef() {
        ColumnReference ref = ColumnReference
                .getAnonymousRowRef(new DbIdentifier("dbName"), new FixtureIdentifier("fixtureName"),
                        new TableIdentifier("anonTable"), new ColumnIdentifier("colName"));
        assertSame(this.anonRowValue, this.resolver.resolve(ref));
    }
}
