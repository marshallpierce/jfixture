/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class RowDependencyEdgeTest {

    @Test
    public void testEndCantBeStart() {
        RowDependencyNode node = new RowDependencyNode(null);

        try {
            new RowDependencyEdge(node, node);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("The start and end cannot be the same", e.getMessage());
        }
    }

    @Test
    public void testEqualsSameNodes() {
        RowDependencyNode node1 = new RowDependencyNode(null);
        RowDependencyNode node2 = new RowDependencyNode(null);

        RowDependencyEdge edge1 = new RowDependencyEdge(node1, node2);
        RowDependencyEdge edge2 = new RowDependencyEdge(node1, node2);

        assertTrue(edge1.equals(edge2));
        assertEquals(edge1.hashCode(), edge2.hashCode());
    }

    @Test
    public void testNotEqualsNull() {
        RowDependencyNode node1 = new RowDependencyNode(null);
        RowDependencyNode node2 = new RowDependencyNode(null);

        RowDependencyEdge edge1 = new RowDependencyEdge(node1, node2);

        //noinspection ObjectEqualsNull
        assertFalse(edge1.equals(null));
    }

    @Test
    public void testNotEqualsDifferentEndNodes() {
        RowDependencyNode node1 = new RowDependencyNode(null);
        RowDependencyNode node2 = new RowDependencyNode(null);
        RowDependencyNode node3 = new RowDependencyNode(null);

        RowDependencyEdge edge1 = new RowDependencyEdge(node1, node2);
        RowDependencyEdge edge2 = new RowDependencyEdge(node1, node3);

        assertFalse(edge1.equals(edge2));
        assertFalse(edge1.hashCode() == edge2.hashCode());
    }

    @Test
    public void testNotEqualsDifferentStartNodes() {
        RowDependencyNode node1 = new RowDependencyNode(null);
        RowDependencyNode node2 = new RowDependencyNode(null);
        RowDependencyNode node3 = new RowDependencyNode(null);

        RowDependencyEdge edge1 = new RowDependencyEdge(node1, node2);
        RowDependencyEdge edge2 = new RowDependencyEdge(node3, node2);

        assertFalse(edge1.equals(edge2));
        assertFalse(edge1.hashCode() == edge2.hashCode());
    }
}
