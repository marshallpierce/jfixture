Jfixture is a tool for loading data (typically for tests) into a db without hardcoding row ids. This lets you build complex multi-table representations of data without manually maintaining ids.

Check out the [documentation here](http://software.mpierce.org/jfixture/).
