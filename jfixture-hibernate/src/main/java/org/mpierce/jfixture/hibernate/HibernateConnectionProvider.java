/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture.hibernate;

import net.jcip.annotations.ThreadSafe;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.jdbc.Work;
import org.jetbrains.annotations.NotNull;
import org.mpierce.jfixture.ConnectionProvider;
import org.mpierce.jfixture.FixtureException;
import org.mpierce.jfixture.SqlAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * ConnectionProvider implementation that uses a Hibernate SessionFactory.
 */
@ThreadSafe
public final class HibernateConnectionProvider implements ConnectionProvider {
    private final SessionFactory sessionFactory;

    private static final Logger logger = LoggerFactory.getLogger(HibernateConnectionProvider.class);

    /**
     * @param sessionFactory the session factory to use
     */
    public HibernateConnectionProvider(@NotNull SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public <T> T performAction(@NotNull SqlAction<T> action) {
        SqlActionWork<T> work = new SqlActionWork<T>(action);

        try {
            final Session session = sessionFactory.openSession();
            try {
                final Transaction tx = session.beginTransaction();
                try {
                    session.doWork(work);
                    tx.commit();
                    session.close();
                } finally {
                    try {
                        if (!tx.wasCommitted()) {
                            tx.rollback();
                        }
                    } catch (HibernateException e) {
                        logger.warn("Failed to rollback", e);
                    }
                }
            } finally {
                try {
                    if (session.isOpen()) {
                        session.close();
                    }
                } catch (HibernateException e) {
                    logger.warn("Couldn't close session", e);
                }
            }
        } catch (HibernateException e) {
            throw new FixtureException("Hibernate error", e);
        }

        return work.getResult();
    }

    /**
     * Helper class to wrap SqlAction execution in a Hibernate Work object.
     *
     * @param <T>
     */
    private static final class SqlActionWork<T> implements Work {

        private final SqlAction<T> action;

        private T result = null;

        private SqlActionWork(SqlAction<T> action) {
            this.action = action;
        }

        @Override
        public void execute(Connection connection) throws SQLException {
            this.result = this.action.doAction(connection);
        }

        private T getResult() {
            return this.result;
        }
    }
}
