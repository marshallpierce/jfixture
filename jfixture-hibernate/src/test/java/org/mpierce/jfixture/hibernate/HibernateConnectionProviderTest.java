/*
 * Copyright © 2010, Marshall Pierce
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.mpierce.jfixture.hibernate;

import org.hibernate.cfg.AnnotationConfiguration;
import org.mpierce.jfixture.ConnectionProviderTestCase;
import org.mpierce.jfixture.FixtureController;

public class HibernateConnectionProviderTest extends ConnectionProviderTestCase {

    @Override
    protected FixtureController setUpFixtureController() {
        FixtureController fc = new FixtureController();
        AnnotationConfiguration config1 = new AnnotationConfiguration();
        config1.setProperty("hibernate.connection.url", DB1_URL);
        config1.setProperty("hibernate.connection.driver_class", DRIVER_CLASS);

        fc.addDb("db1", new HibernateConnectionProvider(config1.buildSessionFactory()));

        AnnotationConfiguration config2 = new AnnotationConfiguration();
        config2.setProperty("hibernate.connection.url", DB2_URL);
        config2.setProperty("hibernate.connection.driver_class", DRIVER_CLASS);

        fc.addDb("db2", new HibernateConnectionProvider(config2.buildSessionFactory()));

        return fc;
    }
}
